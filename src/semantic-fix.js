var fs = require('fs');

// relocate default config
fs.writeFileSync(
  'styles/ui/theme.config',
  "@import 'styles/ui/theme.config';\n",
  'utf8'
);

// fix well known bug with default distribution
fixFontPath('styles/ui/themes/default/globals/site.variables');
fixFontPath('styles/ui/themes/flat/globals/site.variables');
fixFontPath('styles/ui/themes/material/globals/site.variables');

function fixFontPath(filename) {
  var content = fs.readFileSync(filename, 'utf8');
  var newContent = content.replace(
    "@fontPath  : '../../themes/",
    "@fontPath  : '../../../themes/"
  );
  fs.writeFileSync(filename, newContent, 'utf8');
}
