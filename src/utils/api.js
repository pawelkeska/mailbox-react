import axios from 'axios';
import cookie from 'cookies-js';
import { default as config } from '../config';
import NoticeStore from 'components/Notice/store';

const checkHttpStatus = code => code >= 200 && code < 300;

const validateToken = (error) => {
  if(error.response.data.token != undefined)
    switch (error.response.data.token) {
      case 'expired_token':
      case 'invalid_token':
        alert("zly token..");
        break;
    }
    throw error;
};

const refreshToken = response => {
  let { data } = response;

  //if (data.token != undefined) {
  //  cookie.set('token', token);
  //}
  return data;
};

export default url => {
	url = `${config.urlApi}${url}`;
	let token = cookie.get('token');

	const headers = {

	};

	if (token != undefined) {
		headers['Authorization'] = 'Bearer ' + token;
	}

  //headers['Cookie'] = 'vrhn13nbn04h3tdtndvna7dq3';

  axios.defaults.withCredentials = true;
	let $create = (method, url, headers) => data => axios({
    method,
    url,
    data,
    headers,
    validateStatus: checkHttpStatus
  })
  //.catch(validateToken)
  .then(refreshToken)
  /*.catch(function (error) {
    if(!error.status)
      NoticeStore.push({
        'type' : 'error',
        'text' : 'Wystąpiły problemy z połączeniem do serwera. Proszę spróbować później'
      })
  });*/

	return {
		get: $create('get', url, headers),
		post: $create('post', url, headers),
		patch: $create('patch', url, headers),
		put: $create('put', url, headers),
		delete: $create('delete', url, headers)
	};
};
