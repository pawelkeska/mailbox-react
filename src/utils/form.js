export const errorsFromBackend = (errors, form) => {
  errors.forEach(function(error, index){
    form.$(error.field).invalidate(error.message);
  });
};
