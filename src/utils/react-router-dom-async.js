import React, {Component} from 'react';
import {reaction, observable} from 'mobx';
import {observer} from 'mobx-react';
import {Route as ReactRoute} from 'react-router-dom';
import {PulseLoader} from 'halogen';
import AppStore from '../stores/App';
const styles = {
	loader: {
		textAlign: 'center',
		position: ' relative',
		top: '15px'
	}
}
@observer
class AsyncComponent extends Component {
	@observable isLoaded = false;
	@observable isLoading = false;
	component = false;

	constructor(props) {
		super(props);
		/*reaction(
			() => this.isLoading,
			loading => AppStore.isLoading = loading
		);*/
	}

	componentWillMount() {
		this.isLoading = true;
		this.isLoaded = false;
	}

	shouldComponentUpdate(nextProps, nextState) {
		let oldUrl = this.props.location.pathname
		let actualUrl = nextProps.location.pathname

		if(oldUrl != actualUrl)
		{
			return true
			/*this.isLoading = true;
			this.isLoaded = false;

			window.asyncLoad && clearTimeout(window.asyncLoad);
			window.asyncLoad = setTimeout(() => {
				this.props.component.then((module) => {
					this.component = module.default;
					this.isLoading = false;
					this.isLoaded = true;
				})
			}, 1);*/
		}else
			return false
	}

	componentDidMount() {

		window.asyncLoad && clearTimeout(window.asyncLoad);
		window.asyncLoad = setTimeout(() => {
			this.props.component.then((module) => {
				this.component = module.default;
				this.isLoading = false;
				this.isLoaded = true;
			})
		}, 1);
	}

	render() {
		if (this.isLoaded) {
			return <this.component {...this.props} />
		}
		return (
			<div style={styles.loader}>
				<PulseLoader color="#31C400" size="1.5em" />
			</div>
		)
	}
}
export const Route = parentProps => (
	<ReactRoute
		{...parentProps}
		render={props => (
				<AsyncComponent
					{...props}
					component={parentProps.fetch}
				/>
			)
		}
	/>
)
