export function parseUrlToParams(route) {
  let matchesRoute = route.substr(1).split('/');

  return {
    idMailbox: matchesRoute[0],
    action: matchesRoute[1],
    folder: matchesRoute[2],
  }
}
