let baseUrl = 'https://rserwis.nckgrupa.pl/backend';

export default {
  version: '2.0-beta-0-0-1',
  urlApi: baseUrl+'/',
  urlImage: baseUrl+'/images/',
  filesUrl:baseUrl+'/../resources/',
  languages: [
    {
      'symbol': 'pl',
      'title': 'Polski'
    },
    {
      'symbol': 'en',
      'title': 'Angielski'
    }
  ]
}
