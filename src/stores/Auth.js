import { reaction, computed, observable, action } from 'mobx';
import _ from 'lodash';
import api from '../utils/api';
import cookie from 'cookies-js';
import AppStore from './App';
import { errorsFromBackend } from '../utils/form';

export default new class AuthStore {
  @observable isAuthenticated = false;
  @observable isAuthenticating = false;
  @observable user;
  @observable permissions = [];
  @observable isAdmin = false;
  @observable isUser = false;

  constructor() {
    const self = this;
    reaction(
      () => self.isAuthenticating,
      loading => AppStore.isLoading = loading
    );

    let hot = { auth: { isAuthenticated: !!cookie.get('token') } };
    let {auth: {user, isAuthenticated}} = window.__INITIAL__ || hot;
    this.isAuthenticated = isAuthenticated;
    this.user = user != undefined ? user : {};
  }

  @computed get userName() {
    return this.user.username;
  }

  @action async checkAuthByToken () {
    let token = cookie.get('token')
    if(token) {
      this.isAuthenticating = true;

      try {
        let user = await api('auth/get-auth/'+token).get();
        this.user = user.model;
        this.permissions = user.permissions;
        this.isUser = user.isUser;
        this.setIsAdmin();

        this.isAuthenticated = true;
        this.isAuthenticating = false;
      } catch (err) {
        console.log(err);
        this.isAuthenticated = false;
        this.isAuthenticating = false;
        cookie.set('token', undefined);
      }
    }
  }

  @action check (route) {
    if(this.isAdmin)
      return true;

    let matches = route.split('/'),
        hasAccess = false,
        matchesRoute = '';

    matches.splice(0, 1);

    matches.map((value, key) => {
      matchesRoute += '/'+value;
        _.find(this.permissions, (permission) => {
          if(permission == ( key == matches.length-1 ? matchesRoute : matchesRoute+'/*' )){
            hasAccess = true;
            return;
          }
        })
    })

    return hasAccess;
  }

  @action logout(redirect) {
    cookie.expire('token')
    this.isAuthenticated = false
    redirect()
  }

  @action async authenticate(form) {
    this.isAuthenticating = true;

    try {
      let user = await api('auth/login').post(form.values());
      cookie.set('token', user.token);
      this.user = user.model;
      this.permissions = user.permissions;
      this.isUser = user.isUser;
      this.setIsAdmin();

      this.isAuthenticated = true;
      this.isAuthenticating = false;
    } catch (err) {
      console.log(err);
      this.isAuthenticated = false;
      this.isAuthenticating = false;

      errorsFromBackend(err.response.data, form);
    }
  }

  setIsAdmin () {
    let isAdmin = false;
    _.find(this.permissions, (permission) => {
      if(permission == '/*'){
        isAdmin = true;
        return;
      }
    })

    this.isAdmin = isAdmin;
  }
}
