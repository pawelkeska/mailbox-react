import {observable, action, reaction} from 'mobx';
import AppStore from 'stores/App';

// Utils
import api from '../utils/api';
import { errorsFromBackend } from '../utils/form';

import NoticeStore from 'components/Notice/store';

export default class Model{
  @observable isLoading = false;
  @observable model = {};

  constructor() {
    const self = this;
    reaction(
      () => this.isLoading,
      loading => AppStore.isLoading = loading
    );
  }

  @action async getModel (data, initForm = false) {
    this.isLoading = true;
    let response = await api(this.urlsToCrud.getModel+'/'+this.getParam(data)).get();
    this.model = response.model;
    this.model.index = data.index;

    if(initForm)
      initForm(response);
    this.isLoading = false;

    return new Promise((resolve, reject) => {
      if(response.length > 1)
        resolve(resposne);
      else
        resolve(response.model);
    });
  }

  @action async create (form) {
    this.isLoading = true;

    try {
      let model = await api(this.urlsToCrud.create).post(form.values());
      form.clear();
      this.isLoading = false;
      this.addGridRow(model);
    } catch (err) {
      console.log(err);
      this.isLoading = false;
      errorsFromBackend(err.response.data, form);
    }
  }

  @action async update (form) {
    this.isLoading = true;

    try {
      let model = await api(this.urlsToCrud.update+'/'+this.getParam(this.model)).put(form.values());
      this.isLoading = false;
      return new Promise((resolve, reject) => {
        resolve(model);
      });
    } catch (err) {
      console.log('dzialaa error');
      this.isLoading = false;
      errorsFromBackend(err.response.data, form);
    }
  }

  @action async delete (data, reload) {

    try {
      let model = await api(this.urlsToCrud.delete+'/'+this.getParam(data)).delete();
      reload(false, true)
      NoticeStore.push({
        'type' : 'success',
        'text' : 'Pozycja została pomyślnie usunięta'
      })
    } catch (err) {
      console.log(err);
      NoticeStore.push({
        'type' : 'error',
        'text' : 'Błąd podczas usuwania'
      })
    }
  }

  @action updateGridRow (values) {
    this.grid.updateRow(this.model.index, values);
  }

  @action addGridRow (values) {
    this.grid.addRow(values);
  }

  getParam (data) {
    return this.nameParamToCrud != undefined ? data[this.nameParamToCrud] : data.id
  }
}
