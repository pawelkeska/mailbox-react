import {action, observable} from 'mobx';
import _ from 'lodash';

export default new class ModalStore {
  @observable items = []

  @action push(component, props = {}) {
    let uuid = Math.round(Math.random() * 1000000);

    this.items.push({
      uuid,
      component,
      props
    });
  }

  close(uuid = false) {
    _.remove(this.items, { uuid });
  }
}
