import { reaction, computed, observable, action } from 'mobx';
import _ from 'lodash';
import api from '../utils/api';
import cookie from 'cookies-js';
import AppStore from './App';
import { errorsFromBackend } from '../utils/form';

export default new class UserStore {
  @observable isLoading = false
  @observable mailboxes = []
  @observable templatesFooter = []

  constructor() {
    /*const self = this;
    reaction(
      () => self.isAuthenticating,
      loading => AppStore.isLoading = loading
    );

    let hot = { auth: { isAuthenticated: !!cookie.get('token') } };
    let {auth: {user, isAuthenticated}} = window.__INITIAL__ || hot;
    this.isAuthenticated = isAuthenticated;
    this.user = user != undefined ? user : {};*/
  }

  @computed get userName() {
    return this.user.username;
  }

  @action async getMailboxes () {
      this.isLoading = true;

      try {
        let response = await api('mailbox/mailboxesUser').get();
        this.mailboxes = response.mailboxes
        this.templatesFooter = response.templates_footer
        this.isLoading = false
      } catch (err) {
        console.log(err);
        //alert('dzialaaa');
      }
  }
}
