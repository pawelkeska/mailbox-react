import { observable, action } from 'mobx';
import validatorjs from 'validatorjs';

//languages
import pl from 'i18n/pl';


export default new class AppStore {
  @observable title = false;
  @observable isLoading = false;

  constructor () {
     validatorjs.setMessages('pl', pl.validator);
     validatorjs.useLang('pl');
   }

  @action setTitle(title) {
    this.title = title;
  }
}
