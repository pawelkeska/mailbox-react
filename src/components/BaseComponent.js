import React, { Component } from 'react';
import BreadcrumbStore from './Breadcrumb/store';

export default class BaseComponent extends Component {

  componentWillMount () {
    this.setBreadcrumb();
  }

  setBreadcrumb () {
    BreadcrumbStore.push(this.breadcrumb);
  }
}
