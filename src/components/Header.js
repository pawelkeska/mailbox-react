import React from 'react';
import Breadcrumb from './Breadcrumb';

const Header = props => (
  <div className="wrap-header">
    <Breadcrumb />
    <div className="header">
      <div className="header-title">{ props.title }</div>
      
      {props.actions ? <div className="header-actions">{ props.actions }</div> : ''}
    </div>
  </div>
);

export default Header;
