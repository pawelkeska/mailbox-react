import React, {Component} from 'react';
import {observer} from 'mobx-react';
import classNames from 'classnames';
// Stores
import Store from './store';

@observer
class NoticeContainer extends Component {
  render() {
    return (
      <div className="wrap-notice">
      {
        Store.items.map((item, index) => (
          <div key={index}
            className={classNames({
              'notice': true,
              [item.notice.type]: true
            })}
          >
          { item.notice.text }
          </div>
        ))
      }
      </div>
    )
  }
};

export default NoticeContainer;
