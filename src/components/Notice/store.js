import {action, observable} from 'mobx';
import _ from 'lodash';

export default new class NoticeStore {
  @observable items = [];
  @observable isLoading = false;

  @action push(notice) {
    let uuid = Math.round(Math.random() * 1000000);

    this.items.push({
      uuid,
      notice
    });

    setTimeout( () => {
      this.close(uuid)
    }, 5000)
  }

  close(uuid) {
    _.remove(this.items, { uuid });
  }
}
