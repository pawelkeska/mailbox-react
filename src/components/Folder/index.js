import React, {Component} from 'react';
import { withRouter } from 'react-router';
import {NavLink} from 'react-router-dom';
import {observable} from 'mobx';
import {observer} from 'mobx-react';
import _ from 'lodash';
import { List, Card, Icon  } from 'ui';
import { parseUrlToParams } from 'utils/helpers'

@withRouter
@observer
class Folder extends React.Component {

  render () {
      return (
          <List.Header className={parseUrlToParams(this.props.location.pathname).folder == this.props.item.detailsFolder.path ? "active" : ""}><NavLink to={"/"+this.props.match.params.idMailbox+"/list/"+this.props.item.detailsFolder.path}>{ this.props.item.detailsFolder.name }</NavLink></List.Header>
      );
  };
}
@observer
class FolderItem extends React.Component {

  render () {
      if (this.props.item.childrens)
        return (
          <List.Item><List.Icon name='folder' />
            <List.Content>
              <Folder item={this.props.item}/>
              <FolderContainer data={this.props.item.childrens} />
            </List.Content>
          </List.Item>
        );
      else
        return (
          <List.Item><List.Icon name='folder' />
            <List.Content>
              <Folder item={this.props.item}/>
            </List.Content>
          </List.Item>
        );
  };
}
@observer
export default class FolderContainer extends React.Component {

  render() {
      const items = this.props.data;
      let folderItems = [];
      let index=0;

      if(items)
        items.map((item, key) => folderItems.push(<FolderItem item={item} key={key} />))

      return (
        <List>
          {folderItems}
        </List>
      );
  }
}
