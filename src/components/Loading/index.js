import React from 'react';
import {PulseLoader} from 'halogen';

const styles = {
	loader: {
		padding: '20px',
    position: 'absolute',
		top: '40%',
    left: '48%',
		textAlign: 'center'
	}
}

const Loading = props => {
  const { isLoading } = props

  if(isLoading)
    return <div style={styles.loader}><PulseLoader color="#347df3" size="1.5em" /></div>
  else
    return <div>{ props.children }</div>
}

export default Loading;
