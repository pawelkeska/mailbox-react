import React, { Component } from 'react';
import _ from 'lodash'
import Trigger from 'rc-trigger';
import FontAwesome from 'react-fontawesome';
import { Button, Icon } from 'ui'

export const typesActions = {
  delete: function (action, item, route) {
    let popup;

    function _clickYes(){
      action.value(item);
    }

    function _clickNo(){
      popup.close();
    }

    function _setRef(ref){
      popup = ref;
    }

    return (
        <Trigger
          action={['click']}
          popup={
            <div className="popup-grid-delete">
              Czy napewno chcesz usunąć pozycję ?
              <ul>
                <li className="yes" onClick={_clickYes}>Tak</li>
                <li className="no" onClick={_clickNo}>Nie</li>
              </ul>
            </div>}
          popupAlign={{
            points: ['bc', 'tc'],
            offset: [0, 0],
            overflow: {
              adjustX: 1,
              adjustY: 1,
            },
          }}
          destroyPopupOnHide={true}
          mouseEnterDelay={0.1}
          mouseLeaveDelay={0.1}
          ref={_setRef}
        >
          <Button className="action tooltip" data-title="Usuwanie"><FontAwesome name={action.icon} /></Button>
        </Trigger>
    )
  },
  confirm: function (action, item, route) {
    let popup;

    function _clickYes(){
      action.value(item, route);
      popup.close();
    }

    function _clickNo(){
      popup.close();
    }

    function _setRef(ref){
      popup = ref;
    }

    return (
        <Trigger
          action={['click']}
          popup={
            <div className="popup-grid-confirm">
              { _.isFunction(action.textConfirm) ? action.textConfirm(item) : action.textConfirm }
              <ul>
                <li className="yes" onClick={_clickYes}>Tak</li>
                <li className="no" onClick={_clickNo}>Nie</li>
              </ul>
            </div>}
          popupAlign={{
            points: ['cl', 'tc'],
            offset: [0, 0],
            overflow: {
              adjustX: 1,
              adjustY: 1,
            },
          }}
          destroyPopupOnHide={true}
          mouseEnterDelay={0.1}
          mouseLeaveDelay={0.1}
          ref={_setRef}
        >
          
          <Button 
            className={"action tooltip "+(action.addClass != undefined ? action.addClass(item) : '')} 
            data-title={_.isFunction(action.name) ? action.name(item) : action.name }>
              <FontAwesome 
                name={action.icon != undefined ? action.icon(item) : action.icon} 
              />
            </Button>
        </Trigger>
    )
  }
}
