import React from 'react'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'
import _ from 'lodash'
import { Table } from 'ui'
//type filters
import InputFilter from './filters/input'
import DateFilter from './filters/date'
import SelectFilter from './filters/select'
import {
  AutoControlledComponent as Component,
} from 'ui/lib'

export default class Filter extends Component {

  static propTypes = {
    columns: PropTypes.array,
    actions: PropTypes.object,
    filters: PropTypes.array,
    filtersFromServer: PropTypes.object
  }

  static autoControlledProps = []

  _renderFilter (column, index) {
    const { actions, filters, filtersFromServer, onChangeFilter } = this.props

    return (
      <Table.HeaderCell key={index} {...column.filter.cellOptions}>
        { column.name }
        { column.filter.items ?
          column.filter.items.map((item, index) => {
            return (<TypeFilters
              filters={filters}
              column={item}
              name={item.name}
              type={item.type}
              changeFilter={onChangeFilter}
              fromServer={filtersFromServer}
              key={index}
            />)
          })
          :
          <TypeFilters
            filters={filters}
            column={column}
            name={column.attribute}
            type={column.filter.type}
            changeFilter={onChangeFilter}
            fromServer={filtersFromServer}
        />
      }
      </Table.HeaderCell>
    )
  }

  render() {
    const { columns, actions } = this.props

    return (
      <Table.Row className="ui form">
        {
          columns.map((column, index) => {
            if(_.isUndefined(column.visible))
              column.visible = true

            if(column.visible)
              return this._renderFilter(column, index)
          })
        }
        { 
          actions != undefined ? 
            <Table.HeaderCell 
            className="column-filter" 
            style={{ 'width' : actions.settings.width ?  actions.settings.width : 'auto' }}>
          </Table.HeaderCell> 
        : null }
      </Table.Row>
    )
	}
}

const TypeFilters = props => {

  let switchFilter = () => {
      let { name, type, column, filters, filtersFromServer, changeFilter } = props

      switch (type) {
        case 'input':
          return <InputFilter
            defaultValue={filters[name]}
            onBlur={e => changeFilter(name, e.target.value)}
            placeholder={column.placeholder}
          />
        case 'select':
          return <SelectFilter
            selectedtValue={filters[name]}
            onChangeFilter={value => changeFilter(name, value)}
            column={column}
            optionsFromServer={fromServer}
            isClear={false}
          />
        case 'date':
          return <DateFilter
            selectedtValue={filters[name]}
            onChangeFilter={value =>changeFilter(name, value)}
            placeholder={column.placeholder}
            attr={name}
          />
        default:
          return <InputFilter
            defaultValue={filters[name]}
            onBlur={e => changeFilter(name, e.target.value)}
            placeholder={column.placeholder}
          />
      }
  }


  return (
    <div>
    {
      switchFilter()
    }
    </div>
  )
}
