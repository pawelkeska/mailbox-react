import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { withRouter } from 'react-router'
import { PulseLoader } from 'halogen'
import { Table, Button } from 'ui'
import Filter from './Filter'
import Row from './Row'
import Pagination from './pagination'
import api from 'utils/api'
import {
  AutoControlledComponent as Component,
  getUnhandledProps,
} from 'ui/lib'
import queryString from 'qs'
import classNames from 'classnames';
import Loading from 'components/Loading';

class Grid extends Component {
  items = []
  filters = []
  filtersFromServer = {}
  pagination = {
    currentPage: 1,
    perPage: 25,
    optionsPerPage: [10,25,50]
  }
  unListenRoute

  static propTypes = {
    url: PropTypes.string,
    isLoading: PropTypes.bool,
    defaultIsLoading: PropTypes.bool,
    defaultSelectedRows: PropTypes.array,
    selectedRows: PropTypes.array,
    handleDoubleClickRow: PropTypes.func,
    afterSelect: PropTypes.func,
  }

  static autoControlledProps = [
    'isLoading',
    'selectedRows'
  ]

  constructor(props) {
    super(props)

    this.unListenRoute = props.listen(this._listenRoute.bind(this))

    this.handleChangeFilter = this.handleChangeFilter.bind(this)
    this.handleChangePagination = this.handleChangePagination.bind(this)
    this.reload = this.reload.bind(this)
    this.handleClickRow = this.handleClickRow.bind(this)
  }

  getInitialState () {
    const { url } = this.props

    if(url)
      return {
        isLoading: true,
        selectedRows: []
      }
    else
      return {
        selectedRows: []
      }
  }

  componentDidMount() {
    this.props.onRef(this)
  }
  componentWillUnmount() {
    this.props.onRef(undefined)
  }

  componentWillMount () {
    const { url } = this.props

    this._setFilterFromUrl()
    this._loadData()
  }

  componentWillUnmount () {
    this.unListenRoute()
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(this.state.isLoading != nextState.isLoading || this.props.url != nextProps.url || this.state.selectedRows != nextState.selectedRows)
		  return true
    else
      return false
	}

  async _loadData () {
    const { url } = this.props

    let response = await api(url+'?page='+this.pagination.currentPage+'&perPage='+this.pagination.perPage+'&'+this._paramsFilter()).get()

    this.items = response.items

    if(response.filters)
      this.filtersFromServer = response.filters

    this.pagination = { ...this.pagination, ...response._meta }
    this.trySetState({ isLoading: false})
  }

  handleChangeFilter (attr, value) {
    this.filters[attr] = value
    this.pagination.currentPage = 1

    this._changeUrl()
  }

  handleChangePagination (page = false, perPage = false) {
    if(page)
      this.pagination.currentPage = page

    if(perPage)
      this.pagination.perPage = perPage

    this._changeUrl()
  }

  handleClickRow(e, rowData) {
    let { handleDoubleClickRow } = this.props

    this._delayedClick = _.debounce(this.doClick.bind(this, rowData), 500)

    if (this.clickedOnce) {
      this._delayedClick.cancel()
      this.clickedOnce = false
      this.props.push('/'+this.props.match.params.idMailbox+'/details/'+rowData.folder+'/'+rowData.uid)
      //handleDoubleClickRow(rowData)
    } else {
      this._delayedClick()
      this.clickedOnce = true
    }
  }

  doClick(rowData, e) {
    let { afterSelect } = this.props
    this.clickedOnce = undefined

    let selected = this.state.selectedRows.slice()

    if(selected.includes(rowData.uid))
    {
      selected = []
    }else{
      selected = []
      selected.push(rowData.uid)
    }

    this.trySetState({
      selectedRows: selected
    });

    afterSelect(selected.length)
  }

  _setFilterFromUrl () {
    let parseRoute = queryString.parse(location.search.slice(1))

    if(parseRoute['perPage'] != undefined)
      this.pagination.perPage = parseInt(parseRoute['perPage'])

    if(parseRoute.page != undefined)
      this.pagination.currentPage = parseInt(parseRoute.page)

    this.filters = []
    if(parseRoute.filters)
      for(let attr in parseRoute.filters){
          this.filters[attr] = parseRoute.filters[attr]
      }
  }

  _listenRoute (location) {
    if(location.pathname == this.props.location.pathname){
      this._setFilterFromUrl()
      this.reload()
    }else
      return false
  }

  _changeUrl () {
    let filters = []

    for (var name in this.filters) {
      filters.push({
        attr: name,
        value: this.filters[name]
      })
    }

    this.props.push(this.props.location.pathname+'?'+'page='+this.pagination.currentPage+'&perPage='+this.pagination.perPage+this._paramsFilter(), {
      filters
    });
  }

  _paramsFilter () {
    let params = '';

    for (var name in this.filters) {
      if(this.filters[name] != '')
        params += '&filters['+name+']='+this.filters[name]
    }

    return params
  }

  reload () {
    this.trySetState({ isLoading: true})

    this._loadData()
  }

  renderContent () {
    const { isLoading, selectedRows } = this.state

    const unhandled = getUnhandledProps(Grid, this.props)
    const rowPropNames = Row.handledProps
    const filterPropNames = Filter.handledProps

    const rest = _.omit(unhandled, rowPropNames)
    const rowProps = _.pick(unhandled, rowPropNames)
    const filterProps = _.pick(unhandled, filterPropNames)

    if(isLoading)
      return (
        <div style={{ 'position' : 'relative'}}>
          <Loading style={{'margin' : '20px 0 20px 0'}} text="Trwa pobieranie" />
        </div>
      )
    else
      return (
        <div>
          <Button
            color="green"
            className="quantity-items"
          >
            Ilość pozycji: <strong>{this.pagination.totalCount}</strong>
          </Button>
          <Table fixed>
            <Table.Header>
              <Filter
                {...filterProps}
                filters={this.filters}
                filtersFromServer={this.filtersFromServer}
                onChangeFilter={this.handleChangeFilter}
              />
            </Table.Header>
            <Table.Body>
            {
              this.items.map((item, index) => {
                return (<Row
                  {...rowProps}
                  key={index}
                  data={item}
                  expressionClass={this.props.expressionClass}
                  reloadGrid={this.reload}
                  filtersFromServer={this.filtersFromServer}
                  eventClickRow={this.handleClickRow}
                  selected={selectedRows}
                  isSelected={this.state.selectedRows.includes(item.uid)}
                />)
              })
            }
            </Table.Body>
          </Table>
          <Pagination settings={this.pagination} changePagination={this.handleChangePagination} />
        </div>
      )
  }

  render() {
    let classNamesGrid = classNames(Object.assign({
			'wrap-grid-normal': true
		}, this.props.class ? this.props.class : {}))

    return (
        <div className={classNamesGrid}>
        { this.renderContent() }
        </div>
    )
  }
}

export default withRouter(Grid)
