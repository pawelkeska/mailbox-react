import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { withRouter } from 'react-router'
import {NavLink} from 'react-router-dom';
import FontAwesome from 'react-fontawesome';
import { typesActions } from '../action/type';
import { Button } from 'ui'

class ActionRow extends Component {
  constructor(props) {
    super(props)

    this._onClick = this._onClick.bind(this);
  }

  render() {
    if(this.props.options.access)
      if(typesActions[this.props.options.type])
    		return (
          this.callTypesAction()
        )
      else
        return (
          this._renderContent()
        )
    else
      return null
	}

  _renderContent () {
    if(this.props.options.link)
      return (
        <NavLink 
          target="_blank" 
          style={{'margin' : '0px', 'padding' : '5px 5px 0 5px'}} 
          exact 
          to={this.props.options.link(this.props.item, this.props.match)}
          className={"ui button small action tooltip "+(this.props.options.addClass != undefined ? this.props.options.addClass(this.props.item) : '')} 
          data-title={this.props.options.name}
          >
	         <FontAwesome name={this.props.options.icon} />
        </NavLink>
      )
    else
      return (
        <Button
          className="action tooltip"
          onClick={this._onClick}
          data-title={this.props.options.name}
        >
            <FontAwesome name={this.props.options.icon} />
        </Button>
      )
  }

  extendItem() {
    //this.props.item.index = this.props.indexRow;
  }

  callTypesAction()
  {
    this.extendItem();
    return typesActions[this.props.options.type](this.props.options, this, this.props.match)
  }

  _onClick () {
    this.extendItem();
    this.props.options.value(this);
  }
};

export default withRouter(ActionRow);
