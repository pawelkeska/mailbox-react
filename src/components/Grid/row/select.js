import React, { Component } from 'react';
import { observer } from 'mobx-react';
import FontAwesome from 'react-fontawesome';
import { typesActions } from '../action/type';
import { Table } from 'ui'

class SelectRow extends Component {
  constructor() {
    super();
    this._onClick = this._onClick.bind(this);
  }

  render() {
    return (
        <Table.Cell
          className="select"
          style={this.props.style}
          onClick={this._onClick}>{ this.props.item.isSelected != undefined && this.props.item.isSelected  ? <FontAwesome name="check-square-o" /> : <FontAwesome name="square-o" /> }
        </Table.Cell>
    )
	}

  _onClick () {
    this.props.store.setSelected(this.props.indexRow, this.props.item.isSelected == undefined || !this.props.item.isSelected ? true : false);
  }
};

export default SelectRow;
