import React, { Component } from 'react';
import { observer } from 'mobx-react';
import _ from 'lodash';
import classNames from 'classnames';
import { Menu } from 'ui'

@observer
export default class Pagination extends Component {
	settings = {}

	componentWillMount () {
		this.setSettings()
	}

	setSettings () {
		const { settings } = this.props

		let tmpSettigns = {},
				diffPages = settings.pageCount - settings.currentPage;

		if(settings.totalCount <= 10)
			tmpSettigns.from = parseInt(settings.currentPage) - 5;
    if(settings.currentPage > 5) {
      tmpSettigns.from = parseInt(settings.currentPage) - 5;
      tmpSettigns.to = diffPages> 5 ? parseInt(settings.currentPage) + 5 : parseInt(settings.currentPage)+diffPages;
    }else{
      tmpSettigns.from = 1;
      tmpSettigns.to = settings.pageCount > 10 ? (parseInt(settings.currentPage) > 5 ? parseInt(settings.currentPage) + 5 : 10) : settings.pageCount;
    }

		this.settings = {...settings, ...tmpSettigns}
	}

	render() {
		const { settings, changePagination } = this.props

    let quantityPages = Math.ceil(this.settings.totalCount/this.settings.perPage),
				pages = [];

    for(let i = this.settings.from; i <= this.settings.to; i++){
        pages.push(<Menu.Item name={i.toString()} active={this.settings.currentPage == i} onClick={() => changePagination(i)} key={i} />);
    }
  		return (
    	<div className="wrap-pagination-grid">
				<Menu pagination style={{'float' : 'left'}}>
					<div className="item quantity-pages">Ilość na stronie</div>
						{
              settings.optionsPerPage.map((option, index) => {
                return (<Menu.Item name={option.toString()} onClick={() => changePagination(false, option)} active={settings.perPage == option} key={index} />)
              })
            }
				</Menu>
				{ quantityPages > 1 ?
				<Menu pagination style={{'float' : 'right'}}>
          <div className="item quantity-pages">Ilość stron: &nbsp;<strong>{quantityPages}</strong></div>
          { pages }
      	</Menu>
				: ''
				}
			</div>
    )
	}
}
