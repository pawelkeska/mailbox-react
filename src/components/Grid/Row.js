import React from 'react'
import _ from 'lodash'
import ActionRow from './row/action';
import { PulseLoader } from 'halogen'
import classNames from 'classnames';
import { Table, Button } from 'ui'
import PropTypes from 'prop-types'
import {
  AutoControlledComponent as Component,
} from 'ui/lib'

export default class Row extends Component {

  static propTypes = {
		item: 	 PropTypes.object,
		defaultItem: PropTypes.object,
    columns: PropTypes.array,
    actions: PropTypes.object,
    filtersFromServer: PropTypes.object,
    eventClickRow: PropTypes.func,
    expressionClass: PropTypes.func,
    isSelected: PropTypes.bool
  }

  static autoControlledProps = [
    'item',
  ]

	getInitialState () {
    const { data } = this.props

    return {
      item: data
    }
  }

	changeRow (newItem, reloadGrid = false) {
    if(newItem)
		  this.trySetState({item: newItem})

    if(reloadGrid){
      let { reloadGrid } = this.props
      reloadGrid()
    }
	}

  renderAction () {
    const { width, actions, index, filtersFromServer } = this.props
    const { item } = this.state

		return (
			<Table.Cell style={{width: width+'%'}} className="row-cell cell-actions">
				<Button.Group size="small" color="red">
				{
					actions.items.map((action, i) => {
						return <ActionRow
              key={i}
              item={item}
              options={action}
              indexRow={index}
              changeRow={this.changeRow.bind(this)}
              filtersFromServer={filtersFromServer}
            />
					})
				}
		    </Button.Group>
			</Table.Cell>
		)
	}

	renderRow (column, index) {
		const { item } = this.state
    const { filtersFromServer } = this.props

		if(column.value instanceof Function)
			return (
				<Table.Cell
					style={{width:column.width+'%'}}
					className={classNames({
						'row-cell' : true,
						['cell-'+column.attribute] : true
					})}
					key={index}
				>
					{ column.value(item, column.filter.optionsFromServer != undefined && column.filter.optionsFromServer ? filtersFromServer[column.attribute] : false ) }
				</Table.Cell>
			)
		else
			return (
				<Table.Cell
					style={{width:column.width+'%'}}
					className={classNames({
						'row-cell' : true,
						['cell-'+column.attribute] : true
					})}
					key={index}
				>
					{ item[column.attribute] }
				</Table.Cell>
			)
	}

	render () {
		const { item } = this.state,
          { expressionClass } = this.props,
          { eventClickRow, isSelected } = this.props,
          rowClassNames = classNames(Object.assign({
            'selected' : isSelected
        	}, expressionClass ? expressionClass(item) : {}));
		return (
	    <Table.Row
	      style={this.props.style}
        onClick={
          (e) => {
            let iconsActions = document.getElementsByClassName('action-grid-icon')[0]

            if(e.target.contains(iconsActions))
              return null
            else
              eventClickRow(e, item)
          }
        }
        className={rowClassNames}
	    >
	      {
	        this.props.columns.map((column, index) => {
						if(_.isUndefined(column.visible))
							column.visible = true

						if(column.visible)
							return this.renderRow(column, index)
	        })
	      }
				{ this.props.actions ? this.renderAction() : null }
	    </Table.Row>
	  )
	}
}
