import React, { Component } from 'react';
import {observable} from 'mobx';
import { observer } from 'mobx-react';
import DatePicker from 'react-datepicker';
import moment from 'moment';

@observer
export default class DateFilter extends Component {

  constructor (props) {
    super(props)
    this._handleChange = this._handleChange.bind(this);
  }

  _handleChange(date) {
    let { attr, onChangeFilter } = this.props

    if(date != null)
      onChangeFilter(date.format('DD-MM-YYYY'))
    else
      onChangeFilter('')
  }

  render() {
    let { selectedtValue, placeholder } = this.props

    return (
      <div className="filter-date">
        <DatePicker
          dateFormat="DD-MM-YYYY"
          selected={ selectedtValue != '' && selectedtValue !=undefined ? moment(selectedtValue, 'DD-MM-YYYY') : '' }
          onChange={ this._handleChange }
          placeholderText={placeholder}
          withPortal
        />
      </div>
    )
  }
};
