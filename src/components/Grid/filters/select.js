import React, { Component } from 'react';
import {observable} from 'mobx';
import { observer } from 'mobx-react';
import Multiselect from 'components/Form/Select';

@observer
export default class SelectFilter extends Component {
  @observable date

  constructor (props) {
    super(props)
    this._handleChange = this._handleChange.bind(this);
  }

  _handleChange(value) {
    this.props.store.setFilterValue(this.props.column.attribute, value.toString())
  }

  _renderContent () {
    //if(!this.props.store.loadingFilters)
      return (
        <Multiselect
          field={{value: this.props.selectedtValue}}
          options={this.props.column.filter.optionsFromServer ? this.props.optionsFromServer[this.props.column.attribute] : this.props.column.filter.options}
          single={true}
          afterSelect={ this.props.onChangeFilter }
        />
      )
    //else
      //return (<div className="loading-filter">Trwa ładowanie opcji</div>)
  }

  render() {
    return (
      <div className="filter-select">
        { this._renderContent() }
      </div>
    )
  }
};
