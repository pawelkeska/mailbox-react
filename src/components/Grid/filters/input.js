import React from 'react';

const InputFilter = (props) => {
  return (
    <div className="filter-input">
      <input className="ui input" type="text" {...props} />
    </div>
  )
};

export default InputFilter;
