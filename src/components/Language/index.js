import React, { Component } from 'react'
import {observer, inject} from 'mobx-react'
import {observable, reaction} from 'mobx'
import I18n from 'i18n-harmony'
import { Dropdown } from 'ui'
import configSystem from 'config'
import LanguageStore from './store'
import SettingStore from 'containers/settings/stores/system'
import { withRouter } from 'react-router'

function defaultInjectFn() {
  return {
    i18n: LanguageStore.i18n
  };
}

var config = {
  injectFn: defaultInjectFn
};

function translatableForm(Component, type){

  Component.prototype.t = function(field, option) {
    var i18n = LanguageStore.i18n;

    return I18n.t((type ? type+'.' : '')+(field+(option ? '.'+option : '')), false, i18n.locale);
  };

  //inject(config.injectFn)(Component);

  return Component;
}

export function translateForm(type) {
  if(typeof type == 'string'){
    return function(Component) {
      return translatableForm(Component, type);
    }
  }else
    return translatableForm(type);
}

function translatable(Component, type) {

  if (Component.propTypes) {
    Component.propTypes.i18n = React.PropTypes.object;
  } else {
    Component.propTypes = {
      i18n: React.PropTypes.object
    };
  }

  Component.prototype.t = function(key, opts) {
    var i18n = this.props.i18n;

    return I18n.t((type ? type+'.' : '')+key, opts, i18n.locale);
  };

  Component.prototype.has = function(key, opts, includeDefault = false) {
    var i18n = this.props.i18n;

    return I18n.has((type ? type+'.' : '')+key, opts, includeDefault, i18n.locale);
  };

  var observed = observer(Component);
  return inject(config.injectFn)(observed);
}

export function translate(type) {
  if(typeof type == 'string'){
    return function(Component) {
      return translatable(Component, type);
    }
  }else
    return translatable(type);
}

@observer
@withRouter
export class LanguageSelect extends Component {

  constructor (props) {
    super(props)

    this.changeLang = this.changeLang.bind(this)
  }

  changeLang (e, data) {
    if(LanguageStore.i18n.locale != data.value){
      LanguageStore.setLocale(data.value)

      if(this.props.location.pathname === '/settings' || this.props.location.pathname === '/')
        SettingStore.get()
    }
  }

  _getLanguages () {
    let languages = []

    configSystem.languages.map((lang, index) => {
      languages.push({ key: lang.title, text: lang.title, value: lang.symbol})
    })

    return languages;
  }

  render () {
    return (
      <div style={{'display' : 'inline-block', 'marginTop' : '5px'}}>
      <Dropdown
          search
          floating
          labeled
          button
          className='icon'
          icon='world'
          defaultValue={LanguageStore.i18n.locale}
          options={this._getLanguages()}
          onChange={this.changeLang}
        />
      </div>
    )
  }
}
