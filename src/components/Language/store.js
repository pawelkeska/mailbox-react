import { reaction, computed, observable, action, asMap } from 'mobx';
import I18n from 'i18n-harmony';
import validatorjs from 'validatorjs';
//languages
import en from 'i18n/en';
import pl from 'i18n/pl';

export default new class LanguageStore {
  @observable i18n = {
    locale: 'pl'
  }

  constructor () {
    I18n.init({
      translations: {
        en,
        pl
      }
    })

    validatorjs.setMessages('pl', pl.validator);
    validatorjs.setMessages('en', en.validator);
    validatorjs.useLang(this.i18n.locale);
  }

  @action setLocale (lang) {
    this.i18n.locale = lang
    validatorjs.useLang(lang);
  }
}
