import React from 'react';
import _ from 'lodash';

const Loading = props => {
  return (
    <div className="loading-mailbox" style={props.style}>
      { props.text ? <span className="text">{ props.text }</span> : '' }
    </div>
  )
}

export default Loading;
