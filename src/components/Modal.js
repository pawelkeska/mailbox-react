import React, {Component} from 'react';
import classNames from 'classnames';
import {PulseLoader} from 'halogen';

const styles = {
	loader: {
		padding: '20px',
		marginTop: '40%',
		textAlign: 'center'
	}
};

const Modal = props => {
	if(!props.isLoading)
		return (
			<div className="window-wrapper">
				<div className={classNames({ window: true, [`window-size-${props.size}`]: props.size })}>
					<div className="window-header">
						<div className="window-header-title">{props.title}</div>
						<div className="window-header-actions">
							<span className="window-header-action" onClick={props.onClose}>&times;</span>
						</div>
					</div>
						{ props.children }
				</div>
			</div>
		)
	else
		return (
			<div className="window-wrapper">
				<div className={classNames({ window: true, [`window-size-${props.size}`]: props.size })}>
					<div className="window-content">
						<div style={styles.loader}>
							<PulseLoader color="#347df3" size="1.5em" />
						</div>
					</div>
				</div>
			</div>
		)
};

export default Modal;
