import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {NavLink} from 'react-router-dom';
import FontAwesome from 'react-fontawesome';
import { translate } from 'components/Language';
// Stores
import BreadcrumbStore from './store';

@translate
class Breadcrumb extends Component {
  render() {
    if(BreadcrumbStore.items.length > 0)
      return (
        <div className="wrap-breadcrumb">
          <FontAwesome name="sitemap" />
          <ul className="breadcrumb">
          {
            BreadcrumbStore.items.map((item, index) => (
              <li key={index}>
                <NavLink to={"/"+item.route}>
                   { this.t(item.name) }
                </NavLink>
                {index != BreadcrumbStore.items.length-1 ? <span className="separator"><FontAwesome name="angle-right" /></span> : ''}
              </li>
            ))
          }
        </ul>
      </div>
      )
    else
      return (<div></div>)
  }
};

export default Breadcrumb;
