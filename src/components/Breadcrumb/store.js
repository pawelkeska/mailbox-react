import {action, observable} from 'mobx';
import _ from 'lodash';

export default new class BreadcrumbStore {
  @observable items = [];

  @action push(routes) {

    /*let existRoute = _.findIndex(this.items, function(elem) { return elem.route == item.route; });

    console.log(existRoute);

    if(existRoute != -1){
      this.items = _.slice(this.items, 0, existRoute);
      this.items.push(item);
    }else{
      //this.items = [];
      this.items.push(item);
    }*/

    this.items = routes;
  }

  @action remove (route) {
    _.remove(this.items, { route })
  }
}
