import { Field } from 'mobx-react-form'

export default class FieldForm extends Field {

  bind(props = {}) {
    let value = this.state.bindings.load(this, this.bindings, props);

    if(this.state.form.translate){
      value.label = this.state.form.t(value.name);
      value.placeholder = this.state.form.t(value.name+'_placeholder');
    }

    return value;
  }

}
