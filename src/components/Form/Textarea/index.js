import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Form, Message } from 'ui'

@observer
export default class Textarea extends Component {

	render() {
    let field = this.props.field.bind({ type: 'textarea'})

		return (
      <div>
				<Form.Input
					label={field.label}
					error={this.props.field.error ? true : false}
					rows={this.props.rows ? this.props.rows : 5}
					control="textarea"
					{...field}
				/>
				<Message
		      error
		      content={this.props.field.error}
		    />
      </div>
    )
	}
}
