import _ from 'lodash'
import { observer } from 'mobx-react'
import React, { Component } from 'react'
import { Search, Grid, Header, Message } from 'ui'
import api from 'utils/api'

@observer
export default class DropdownSearch extends Component {
    field

    constructor (props) {
        super(props)

        this.resetComponent = this.resetComponent.bind(this)
        this.handleResultSelect = this.handleResultSelect.bind(this)
        this.handleSearchChange = this.handleSearchChange.bind(this)
    }

    componentWillMount() {
        this.resetComponent()
    }

  resetComponent = () => this.setState({ isLoading: false, results: [], value: '' })

    handleResultSelect = (e, { result }) => {
        let emails = this.state.value.split(','),
            value = ''
        emails.pop()
        value = emails.length >= 1 ? emails.join(', ')+', '+result.title : result.title
        this.props.field.set(value)
        this.setState({ value })
    }

    async handleSearchChange (e, { value }) {
        let emails = value.split(',');
        this.props.field.set(value)

        this.setState({ isLoading: true, value })

        if(emails.length > 1)
            value = emails.pop().trim();

        if (value.length < 1) {
            this.setState({
                isLoading: false,
                results: []
            })
            return;
        }

        try {
            let results = await api('system/mailboxAddressBook/search?query='+value).get()
            
            this.setState({
                isLoading: false,
                results
            })
        } catch (err) {

        }
    }

  render() {
    this.field = this.props.field.bind({ type: this.props.type ? this.props.type : 'text'})
    const { isLoading, value, results } = this.state

    return (
        <div className={'field' + (this.props.field.error ? ' error' : '')}>
            <label>{ this.field.label }</label>
            <div>
                <Search
                    loading={isLoading}
                    onResultSelect={this.handleResultSelect}
                    onSearchChange={_.debounce(this.handleSearchChange, 500, { leading: true })}
                    results={results}
                    value={this.field.value}
                    noResultsMessage='Brak e-maili'
                />
                <Message
                    error
                    content={this.props.field.error}
                />        
            </div>
        </div>
    )
  }
}