import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider, observer } from 'mobx-react';
import FontAwesome from 'react-fontawesome';
import Option from './option';
import _ from 'lodash';
//stores
import Store from './store';
//import GridStore from './store';
//import Row from './row';
//import { AutoSizer, List } from 'react-virtualized';
//import { PulseLoader } from 'halogen';

@observer
export default class Multiselect extends Component {
  results: []

	constructor (props) {
		super(props);

    this.store = new Store(this.setSelected(props.field.options && _.isArray(props.field.options) > 0 ? props.field.options : props.options))
    this._unMarkAll = this._unMarkAll.bind(this)
    this._showSelect = this._showSelect.bind(this)
    this._afterSelect = this._afterSelect.bind(this)
    this._handleClickOutside = this._handleClickOutside.bind(this)
    this._clearValue = this._clearValue.bind(this)
	}

  componentDidMount() {
    document.addEventListener('click', this._handleClickOutside, true);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this._handleClickOutside, true);
  }

  _handleClickOutside(event) {
    const domNode = ReactDOM.findDOMNode(this);

    if ((!domNode || !domNode.contains(event.target))) {
      this.store.visible = false
    }
  }

  setSelected (options) {
    let selected = [],
        value = this.props.field.value;

    if(!Array.isArray(value))
      value = [this.props.field.value]

    console.log('MULTISELECT VALUE ')
    console.log(this.props.field.value)
    console.log(value)

    options.map( (option) => {
      if(_.find(value, (selected) => option.value == selected)){
        selected.push(option.name)
        option.isSelected = true;
      }else
        option.isSelected = false;
    })

    return {
      options,
      selected
    }
  }

  _unMarkAll () {
    this.store.options.map( (option) => {
      option.isSelected = false
    })
  }

  _showSelect () {
    this.store.visible = !this.store.visible
  }

  _clearValue () {
    this.store.selected = []
    this._unMarkAll()

    if(this.props.afterSelect)
      this.props.afterSelect('')
  }

  _afterSelect (name, val, action = false) {
    if(this.props.single){
      this.store.visible = false
      this.store.selected[0] = name
    }else{
      if(action === 'add'){
        this.store.selected.push(name)
      }else if(action === 'remove'){
        this.store.selected = _.remove(this.store.selected, (item) => {
          return item != name;
        })
      }
    }

    if(this.props.afterSelect)
      this.props.afterSelect(val)
  }

  _renderTitle () {
    if(this.store.selected.length > 0)
      return this.store.selected.join(', ')
    else
      return <span>- wybierz -</span>
  }

	render() {
    let { selected, field, single } = this.props

		return (
      <div className="wrap-select">
        <div className="wrap-select-title">
          <div className="select-title" onClick={this._showSelect}>
            {
              this._renderTitle()
            }
          </div>
          <div onClick={this._clearValue} className="clear"><FontAwesome name="remove" /></div>
        </div>
        <div className="wrap-list">
        <ul style={{'display' : this.store.visible ? 'block' : 'none'}}>
          {
            this.store.options.map( (option, index) => {
              return (
                <Option
                  field={field}
                  singleSelected={single ? single : false}
                  clear={this._unMarkAll}
                  option={option}
                  afterSelect={this._afterSelect}
                  key={index}
                />
              )
            })
          }
        </ul>
        </div>
      </div>
    )
	}
}
