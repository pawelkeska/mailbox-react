import { reaction, computed, observable, action, asMap } from 'mobx';
import _ from 'lodash';

export default class MultiselectStore {
  @observable options = false;
  @observable visible = false;
  @observable selected = [];

  constructor(config) {
    this.options = config.options;
    this.selected = config.selected;
  }
}
