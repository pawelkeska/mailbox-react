import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {NavLink} from 'react-router-dom';
import FontAwesome from 'react-fontawesome';
import classNames from 'classnames';
import _ from 'lodash';

@observer
export default class Option extends Component {

  constructor (props) {
    super(props)
    this._onClick = this._onClick.bind(this);
  }

  _onClick () {
    if(this.props.singleSelected){
      this.props.clear()
      this.props.option.isSelected = true;
      this.props.field.value = this.props.option.value;
      this.props.afterSelect(this.props.option.name, this.props.field.value)
    }else{
      if(this.props.option.isSelected){
        this.props.option.isSelected = false;
        this.props.field.value = _.remove(this.props.field.value, (item) => {
          return item != this.props.option.value;
        });
        this.props.afterSelect(this.props.option.name, this.props.option.value, 'remove')
      }else{
        this.props.option.isSelected = true;

        this.props.field.value = [...this.props.field.value, ...[this.props.option.value]];
        this.props.afterSelect(this.props.option.name, this.props.option.value, 'add')
      }
    }
  }

  render() {
    return (
      <li onClick={this._onClick} className={classNames({ 'selected': this.props.option.isSelected })}>
        { this.props.option.isSelected ? <FontAwesome name="check-square-o" /> : '' }
        { this.props.option.name }
      </li>
    )
  }
};
