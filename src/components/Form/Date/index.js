import React, { Component } from 'react';
import {observable} from 'mobx';
import { observer } from 'mobx-react';
import DatePicker from 'react-datepicker';
import { translate } from 'components/Language';
import moment from 'moment';
import classNames from 'classnames';
import { Message } from 'ui'

@translate
export default class Date extends Component {

	constructor (props) {
    super(props)
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(date) {
    this.props.field.value = date
  }

	render() {
		let field = this.props.field.bind({ type: this.props.type ? this.props.type : 'text'})

		return (
			<div>
	      <div className={classNames({
					'field': true,
					'error' : this.props.field.error
				})}>
	        <label htmlFor={field.id}>
	          { field.label }
	        </label>
					<DatePicker
							dateFormat="YYYY-MM-DD"
							selected={ field.value ? moment(field.value) : field.value }
			        onChange={ this.handleChange }
							locale={ this.props.i18n.locale }
							placeholderText={ field.placeholder }
							className="ui input"
							withPortal
			    />
	      </div>
				<Message
					error
					content={this.props.field.error}
				/>
			</div>
    )
	}
}
