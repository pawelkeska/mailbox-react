import React, { Component } from 'react'
import { observer } from 'mobx-react'
import { Form, Message } from 'ui'

@observer
export default class Input extends Component {

	render() {
    let field = this.props.field.bind({ type: this.props.type ? this.props.type : 'text'})

		return (
      <div>
				<Form.Input label={field.label} {...field} type={this.props.type ? this.props.type : 'text'} error={this.props.field.error ? true : false} />
				<Message
		      error
		      content={this.props.field.error}
		    />
      </div>
    )
	}
}
