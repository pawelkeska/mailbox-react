import MobxReactForm from 'mobx-react-form';
import I18n from 'i18n-harmony';
import LanguageStore from 'components/Language/store';
import Field from './Field';

export default class Form extends MobxReactForm {
  constructor(setup = {}, {

    name = null,
    options = {},
    plugins = {},
    bindings = {},
    onSubmit = {},

  } = {} , translate = false) {
    super(setup, { name, options, plugins, bindings, onSubmit});

    this.translate = translate
    this.modal = {}
  }

  makeField(data) {
    return new Field(data);
  }
}
