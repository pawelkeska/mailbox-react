import React, {Component} from 'react';
import {observable} from 'mobx';
import {observer} from 'mobx-react';
import classNames from 'classnames';
import Config from 'config';
// Components
import FontAwesome from 'react-fontawesome';
import {PulseLoader} from 'halogen';
import Uploader from './uploader';
import { Button, Label } from 'ui'
//stores
import NoticeStore from 'components/Notice/store';

const styles = {
	loader: {
    float: 'right',
    marginRight: '20px'
	}
};

@observer
export default class Upload extends Component {
  //@observable files = []
  //@observable loadingFiles = []

  constructor (props) {
    super(props)

    this._onDrop = this._onDrop.bind(this)
    this._uploadFiles = this._uploadFiles.bind(this)
    this._clearFiles = this._clearFiles.bind(this)
  }

  _onDrop (acceptedFiles, rejectedFiles) {
    acceptedFiles.map((file) => {
      this.props.store.files.push({
        name: file.name,
        objectFile: file,
        isUploaded: false,
        isUploading: false
      })
    })
  }

  async _uploadFiles () {
      this.props.store.upload()
  }

  _clearFiles () {
    this.props.store.files.replace([])
  }

	render() {
		return (
      <div>
        {
          !this.props.store.isUploadingFiles ?
            <Uploader className="uploader" onDrop={this._onDrop}>
              <div>Proszę upuścić pliki lub kliknąć, aby wybrać</div>
            </Uploader>
          : ''
        }

        {
          this.props.store.files.length > 0 ?
          <div className="uploader-list-files">
            <h2>Pliki do wgrania: {this.props.store.files.length}</h2>
              {
                this.props.store.files.map((file, index) =>
                <Label color="brown" className={classNames({
                    'uploaded': file.isUploaded ? true : false,
                    'uploading': file.isUploading
                  })}
                  key={index}
                  >
                    <FontAwesome name="file-o" /> { file.name }
                    { file.isUploading ? <div style={styles.loader}><PulseLoader color="#fff" size="0.7em" /></div> : '' }
                    { file.isUploaded ? <div style={{float: 'right', marginRight: '10px'}}><FontAwesome name="thumbs-o-up" /></div> : '' }
                </Label>
                )
              }
              <div className="wrap-button">
                {
                  this.props.showUploadButton ?
                  <Button
										color="green"
      							type="submit"
      							onClick={this._uploadFiles}
                  >
                    Wgraj pliki
                  </Button>
                  : ''
                }
                <Button
									color="red"
    							type="submit"
    							onClick={this._clearFiles}
                >
                  Wyczyść
                </Button>
              </div>
          </div>
          : null
        }
      </div>
		)
	}
}
