import { Portal as BasePortal } from 'ui'
import {
  AutoControlledComponent as Component,
  isBrowser,
  keyboardKey,
  makeDebugger,
  META,
} from 'ui/lib'

/**
 * A component that allows you to render children outside their parent.
 * @see Modal
 * @see Popup
 * @see Dimmer
 * @see Confirm
 */
class Portal extends BasePortal {

  componentWillMount () {
    this.open()
  }

  render() {
    return null
  }
}

export default Portal
