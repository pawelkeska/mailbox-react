import React, {Component} from 'react';
import _ from 'lodash'
import PropTypes from 'prop-types'
import cx from 'classnames';
import {PulseLoader} from 'halogen';
import { Icon, Modal as BaseModal } from 'ui'
import Portal from './portal'
import ModalHeader from 'ui'
import ModalContent from 'ui'
import ModalActions from 'ui'
import ModalDescription from 'ui'
import {
  getUnhandledProps,
  isBrowser,
  useKeyOnly,
  getElementType,
  childrenUtils
} from 'ui/lib'


const styles = {
	loader: {
		padding: '20px',
    position: 'absolute',
		top: '40%',
    left: '48%',
		textAlign: 'center'
	}
};

class Modal extends BaseModal {

  static propTypes = {
    i18n: PropTypes.object,
    data: PropTypes.object,
    removeFromStore:  PropTypes.func,
    isLoading: PropTypes.bool,
    defaultIsLoading: PropTypes.bool,
    ...BaseModal.propTypes
  }

  static defaultProps =  {
    dimmer: true,
    closeOnDimmerClick: true,
    closeOnDocumentClick: false
  }

  static autoControlledProps = [
    'open',
    'isLoading'
  ]

  async componentWillMount () {
    if(this.getData){
      this.setLoading(true)
      await this.getData()
      this.setLoading(false)
    }

    this.init()
	}

  init () {

  }

  handleClose = (e) => {
    const { onClose, removeFromStore} = this.props
    if (onClose) onClose(e, this.props)

    this.trySetState({ open: false })
    if (removeFromStore) removeFromStore()
  }

  setLoading = (loading) => {
    this.trySetState({ isLoading: loading })
  }

  setPosition = () => {
    if (this.ref) {
      const mountNode = this.getMountNode()
      const { height } = this.ref.getBoundingClientRect()

      const marginTop = -Math.round(height / 2)
      const scrolling = height >= window.innerHeight-30

      const newState = {}

      if (this.state.marginTop !== marginTop) {
        newState.marginTop = marginTop
      }

      if (this.state.scrolling !== scrolling) {
        newState.scrolling = scrolling

        if (scrolling) {
          mountNode.classList.add('scrolling')
        } else {
          mountNode.classList.remove('scrolling')
        }
      }

      if (Object.keys(newState).length > 0) this.setState(newState)
    }

    this.animationRequestId = requestAnimationFrame(this.setPosition)
  }  

  renderContent = rest => {
    const {
      actions,
      basic,
      children,
      className,
      closeIcon,
      content,
      header,
      size,
      style,
    } = this.props
    const { marginTop, scrolling } = this.state

    const classes = cx(
      'ui',
      size,
      useKeyOnly(basic, 'basic'),
      useKeyOnly(scrolling, 'scrolling'),
      'modal transition visible active',
      className,
    )
    const ElementType = getElementType(Modal, this.props)

    const closeIconJSX = Icon.create('close', { overrideProps: this.handleIconOverrides })
    const minimizeIconJSX = Icon.create('window minimize', { overrideProps: this.handleIconOverrides })

    const icons = <div className="icons">{minimizeIconJSX}{closeIconJSX}</div>

    //if (!childrenUtils.isNil(children)) {
      return (
        <ElementType {...rest} className={classes} style={{ marginTop, ...style }} ref={this.handleRef}>
          {icons}
          {this.getHeader()}
          {this.getContent()}
          {this.getActions()}
        </ElementType>
      )
    //}

    /*return (
      <ElementType {...rest} className={classes} style={{ marginTop, ...style }} ref={this.handleRef}>
        {icons}
        {ModalHeader.create(header)}
        {ModalContent.create(content)}
        {ModalActions.create(actions, { overrideProps: this.handleActionsOverrides })}
      </ElementType>
    )*/
  }

  render () {
    const { open, isLoading } = this.state
    const { closeOnDimmerClick, closeOnDocumentClick, dimmer } = this.props
    const mountNode = this.getMountNode()

    // Short circuit when server side rendering
    if (!isBrowser) return null

    const unhandled = getUnhandledProps(Modal, this.props)
    const portalPropNames = Portal.handledProps

    const rest = _.omit(unhandled, portalPropNames)
    const portalProps = _.pick(unhandled, portalPropNames)

    // wrap dimmer modals
    const dimmerClasses = !dimmer
      ? null
      : cx(
        'ui',
        dimmer === 'inverted' && 'inverted',
        'page modals dimmer transition visible active',
      )

		return (
      <Portal
        closeOnDocumentClick={closeOnDocumentClick}
        closeOnRootNodeClick={closeOnDimmerClick}
        {...portalProps}
        className={dimmerClasses}
        mountNode={mountNode}
        open={open}
        onClose={this.handleClose}
        onMount={this.handlePortalMount}
        onOpen={this.handleOpen}
        onUnmount={this.handlePortalUnmount}
      >
        { !isLoading ? this.renderContent(rest) : <div style={styles.loader}><PulseLoader color="#347df3" size="1.5em" /></div> }
      </Portal>
		)
  }
};

export default Modal;
