import React from 'react';
import {NavLink} from 'react-router-dom';
import classNames from 'classnames';
export const NavLinkItem = props => (
  <li className="navbar-list-item">
    <NavLink
      {...props}
      activeClassName="navbar-list-item--active">
      {props.children}
    </NavLink>
  </li>
);
export const NavItem = props => (
  <li className="navbar-list-item">
    <a {...props}>{props.children}</a>
  </li>
);
export const NavList = props => (
  <ul className={classNames({
    "navbar-list": true,
    "navbar-list navbar-list-right": props.right != undefined
  })}>
    {props.children}
  </ul>
);
export const SubNavList = props => (
  <ul className={classNames({
    "navbar-list-sub": true,
    "navbar-list navbar-list-right": props.right != undefined
  })}>
    {props.children}
  </ul>
);
