import React from 'react';
import AuthStore from 'stores/Auth';
import _ from 'lodash';

const CheckAccess = props => {
  let access = _.isBoolean(props.route) ? props.route : AuthStore.check(props.route);
  if(access)
    return (props.children)
  else
    return false
};

export default CheckAccess;
