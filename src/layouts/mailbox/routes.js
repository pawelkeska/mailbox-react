import React, {Component} from 'react';
import {Route} from 'utils/react-router-dom-async';

const Routes = props => (
  <div>
    <Route path="/:idMailbox" fetch={System.import('containers/mailbox')} />
  </div>
);

export default Routes
