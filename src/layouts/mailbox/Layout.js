import React, {Component} from 'react'
import {observer} from 'mobx-react'
import Routes from './routes'
import { withRouter } from 'react-router'
import User from 'stores/User'
import Config from 'config'
import { Button } from 'ui'

@observer
class Layout extends Component {

	componentWillMount()
	{
		if(this.props.location.pathname == '/')
			this.props.push(User.mailboxes[0].id+'/list/INBOX')
	}

	render() {
		return (
			<div>
				<div>
					<Button id="version" floated="right" size="tiny" color="red">
						v. { Config.version }</Button>
					<Routes />
				</div>
			</div>
		)
	}
}

export default withRouter(Layout)
