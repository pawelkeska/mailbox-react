export default {
  validator: {
    accepted: 'Pole musi być zaakceptowane.',
    alpha: 'Pole może zawierać tylko litery.',
    alpha_dash: 'Pole moze zawierać tylko litery, myślnik i podrkeślenie.',
    alpha_num: 'Pole moze zawierac tylko znaki alfanumeryczne.',
    between: 'Pole musi mieć długość od :min do :max.',
    confirmed: 'Pole nie spełnia warunku potwierdzenia.',
    email: 'Pole ma niepoprawny format adresu email.',
    date: 'Pole musi mieć poprawny format daty.',
    def: 'Pole zawiera błędy.',
    digits: 'Pole może zawierać tylko cyfry ze zbioru :digits.',
    different: 'Pole musi się różnić od pola :different.',
    'in': 'Pole musi należeć do zbioru :in.',
    integer: 'Pole musi być liczbą całkowitą.',
    min: {
        numeric: 'Pole musi być równe conajmniej :min.',
        string: 'Pole musi zawierać conajmniej :min znaków.'
    },
    max: {
        numeric: 'Pole nie moze być większe :max.',
        string: 'Pole nie moze być dłuższe niż :max znaków.'
    },
    not_in: 'Pole nie może należeć do zbioru :not_in.',
    numeric: 'Pole musi być liczbą.',
    present: 'Polu musi być obecny (ale może być pusta).',
    required: 'Pole jest wymagane.',
    required_if: 'Pole jest wymagane jeśli pole :other jest równe :value.',
    same: 'Pole musi być takie samo jak pole :same.',
    size: {
        numeric: 'Pole musi być równe :size.',
        string: 'Pole musi zawierać :size znaków.'
    },
    string: 'Pole musi być ciągiem znaków.',
    url: 'Pole musi być poprawnym adresem URL.',
    regex: 'Pole nie spełnia warunku.',
    attributes: {}
  }
}
