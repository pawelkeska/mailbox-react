require("./styles/main.scss");
require("./styles/ui/semantic.less");
import React from "react";
import ReactDOM from "react-dom";
import * as Sentry from '@sentry/browser';
import { AppContainer } from "react-hot-loader";
import App from "./containers/App";

Sentry.init({dsn: "http://25e09227962a4c56b060b229f5548e7e@94.152.212.7:9000/5"});

ReactDOM.render(
  <AppContainer>
    <App />
  </AppContainer>,
  document.getElementById("mailbox")
);

// Hot Module Replacement API
if (module.hot) {
  module.hot.accept("./containers/App", () => {
    const NextApp = require("./containers/App").default;
    ReactDOM.render(
      <AppContainer>
        <NextApp />
      </AppContainer>,
      document.getElementById("mailbox")
    );
  });
}
