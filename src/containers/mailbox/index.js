import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {NavLink} from 'react-router-dom';
import {observer} from 'mobx-react';
import { withRouter } from 'react-router';
import Routes from './routes';

// Stores
import UserStore from 'stores/User';
import MailboxStore from './stores/model';

// Components
import ListFolder from './ListFolder';
import BaseComponent from 'components/BaseComponent';
import { Input, Menu, Segment, Grid } from 'ui';

@observer
class Mailbox extends BaseComponent {

	constructor(props) {
    super(props);
		this.id
    this.breadcrumb = [{
      route: 'user',
      name: 'Użytkownicy'
    }];
  }

	renderMailboxes () {
		return(<div>
			<Menu pointing>
				{
					UserStore.mailboxes.map((mailbox, index) => {
						return (<Menu.Item
							key={index}
							as={Link}
							active={this.props.match.params.idMailbox == mailbox.id}
							to={'/'+mailbox.id+'/list/INBOX'
						}>
							{ mailbox.username } &nbsp;<strong style={{'color' : 'red' }}>({ mailbox.unseen })</strong>
						</Menu.Item>)
					})
				}
				<Menu.Menu position='right'>
					<Menu.Item>
						<Input icon='search' placeholder='Wyszukaj...' />
					</Menu.Item>
				</Menu.Menu>
			</Menu>
			<Grid style={{'margin' : '0 !important', 'padding' : '0 10px 0 10px'}}>
				<Grid.Row>
						<Grid.Column width={4} style={{'paddingRight' : '0px'}}>
							<ListFolder />
						</Grid.Column>
						<Grid.Column width={12}>
							<Routes />
						</Grid.Column>
				</Grid.Row>
			</Grid>
		</div>)
	}

	render() {
		return (
      <div>
				{
					UserStore.mailboxes.length > 0 ? this.renderMailboxes() : 'Brak skrzynek'
				}
			</div>
		)
	}
}

export default withRouter(Mailbox)
