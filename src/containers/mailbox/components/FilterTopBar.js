import React, { Component } from 'react'
import { withRouter } from 'react-router'
import queryString from 'qs'
//ui
import { Segment, Label, Dropdown } from 'ui'


class FilterTopBar extends Component {

    constructor(props) {
        super(props)
    }

    getValue() {
        let parseRoute = queryString.parse(location.search.slice(1)),
            filters = []

        if(parseRoute.filters)
          for(let attr in parseRoute.filters){
              filters[attr] = parseRoute.filters[attr]
          }

        if(filters['visibility']) {
            return filters['visibility']
        }

        return ''
    }


    render() {
        let { changeFilter } = this.props,
            value = this.getValue()
        return (
            <Segment color='red' style={{'padding' : '8px 10px 8px 10px', 'margin' : '5px 0 0 0'}}>
            <Label color="blue">
                Filtry
            </Label>
            
            <Dropdown 
                text='wszystkie'
                button
                className='icon'
                floating
                labeled
                icon='eye'
                options={[
                { key: 0, text: 'nieprzeczytane', value: 'Nie' },
                { key: 1, text: 'przeczytane', value: 'Tak' }
                ]}
                //defaultValue={value}
                value={value}
                text={ value == false ? 'wszystkie' : value == 'Tak' ? 'przeczytane' : 'nieprzeczytane'}
                onChange={(e, data) => changeFilter('visibility', data)}
            >
            </Dropdown>
            </Segment>
        )
    }
}

export default withRouter(FilterTopBar)