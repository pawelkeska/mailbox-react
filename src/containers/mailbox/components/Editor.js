import React, { Component } from 'react'
import {observer} from 'mobx-react';
import { EditorState, ContentState, convertToRaw, convertFromHTML } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import draftToHtml from 'draftjs-to-html'
//ui
import { Message } from 'ui'

@observer
class EditorConvertToHTML extends Component {

    constructor(props) {
        super(props)

        this.onEditorStateChange = this.onEditorStateChange.bind(this)
        
        this.editorState = EditorState.createEmpty()
        this.isChangeByEditor = false

        this.state = {
            editorState: EditorState.createEmpty(),
        }

        this.isChangeFromClick = false
    }

    componentWillUpdate(nextProps, nextState)
    {
        if(this.isChangeFromClick) {
            this.isChangeFromClick = false
        }else{
            let blocksFromHTML = convertFromHTML(nextProps.field.value);
            this.editorState = EditorState.createWithContent(ContentState.createFromBlockArray(
                blocksFromHTML.contentBlocks,
                blocksFromHTML.entityMap
            ))
        }
    }

    onEditorStateChange (editorState) {
        this.editorState = editorState
        this.isChangeFromClick = true
        let content = draftToHtml(convertToRaw(editorState.getCurrentContent()))
        this.props.field.set(content)
        this.setState({
            editorState
        })
    }

    render() {
        this.field = this.props.field.bind({ type: this.props.type ? this.props.type : 'text'})

        return (
            <div className={'field' + (this.props.field.error ? ' error' : '')}>
                <label>{ this.field.label }</label>
                <div>
                    <Editor
                        editorState={ this.editorState}
                        onEditorStateChange={this.onEditorStateChange}
                        localization={{
                            locale: 'pl',
                        }}
                    />
                    <Message
                        error
                        content={this.props.field.error}
                    />        
                </div>
            </div>
        );
    }
}

export default EditorConvertToHTML