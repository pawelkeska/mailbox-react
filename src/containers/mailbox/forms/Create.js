import MobxReactForm from 'mobx-react-form';
import validatorjs from 'validatorjs';
//stores
import User from '../stores/model';

const plugins = { dvr: validatorjs };

class Create extends MobxReactForm {

  onSuccess(form) {
    form.modal.setLoading(true)
    User.create(form).then((res) => {
      form.modal.setLoading(false)
    })
 }

	onError(form) {
    console.log("error");
	}
}

const form = new Create({ fields: {} }, { plugins });
export default form;
