export const fields = (type, response = false) => {
  let fields = {
    display_name: {
      label: 'Nazwa wyświetlana',
      placeholder: 'Wpisz nazwę',
      rules: 'required|string|between:2,32',
    },
    email: {
      label: 'E-mail',
      placeholder: 'Wpisz e-mail',
      rules: 'required|string|between:2,32',
    },
    phone: {
      label: 'Telefon',
      placeholder: 'Wpisz telefon',
    },
    company: {
      label: 'Firma/organizacja',
      placeholder: 'Wpisz firmę/organizację',
    },
    password: {
      label: 'Hasło',
      placeholder: 'Wpisz hasło',
      rules: type == 'create' ? 'required|string|between:1,32' : false,
    },
    role: {
      label: 'Role użytkownika',
      options: response.systemRoles
    }
  }

  if(response.model)
    for (var key in response.model) {
      if(fields[key])
        fields[key].value = response.model[key]
    }

  return fields;
};
