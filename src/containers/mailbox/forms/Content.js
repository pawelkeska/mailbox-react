import React, {Component} from 'react';
import {observer} from 'mobx-react';
import Multiselect from 'components/Form/Select';
import Input from 'components/Form/Input';

const Form = props => {
    let { form } = props;

		return (
      <div>
        <Input field={form.$('display_name')} />
        <Input field={form.$('email')} />
        <Input field={form.$('phone')} />
        <Input field={form.$('company')} />
        <Input field={form.$('password')} type="password" />

        <label htmlFor={form.$('role').id}>
          {form.$('role').label}
        </label>
        <Multiselect field={form.$('role')} />
        <p className="error">{form.$('role').error}</p>
      </div>
    )
};

export default observer(Form);
