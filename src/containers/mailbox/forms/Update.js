import MobxReactForm from 'mobx-react-form';
import validatorjs from 'validatorjs';
//stores
import User from '../stores/model';

const plugins = { dvr: validatorjs };

class Update extends MobxReactForm {

  onSuccess(form) {
    form.modal.setLoading(true)
    User.update(form).then((res) => {
      form.modal.setLoading(false)
    })
 }

	onError(form) {
    console.log("error");
	}
}

const form = new Update({ fields: {} }, { plugins });

export default form;
