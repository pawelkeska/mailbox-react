import React from 'react'
import FontAwesome from 'react-fontawesome'
import moment from 'moment'
import { Icon } from 'ui'
//modals
import UpdateModal from './modals/Update'
import ViewModal from './modals/View'
import ConfirmRegistration from './modals/ConfirmRegistration'
//stores
import AuthStore from 'stores/Auth'
import ModalStore from '../../stores/Modal'
import MailboxStore from './stores/model'

export let expressionRowClass = (data) => {
	let css = {}

	if(data.flags['SEEN'])
		css['seen'] = true
	else
		css['not-seen'] = true

	return css
}

export let columns = (folder) => {
	return [
		{
			name: 'Temat',
			attribute: 'subject',
			sortable: true,
			width:5,
			filter: {
				type: 'input',
				cellOptions: {
					style: {
						width: '99%',
						whiteSpace: 'nowrap'
					}
				}
			},
			value: (data) => {
				return (<div>
					{ data.flags['ANSWERED'] ? <a title="Odpowiedziane"><Icon size='small' name='reply' /></a> : null }				
					{ data.flags['FORWARDED'] ? <a title="Przekazane"><Icon size='small' name='mail forward' /></a> : null }
					{ data.subject }
				</div>)
			},
		},
		{
			name: 'Od',
			attribute: 'from',
			sortable: true,
			width:5,
			filter: {
				type: 'input',
				cellOptions: {
					style: {
						width: '250px'
					}
				}
			},
			visible: folder != MailboxStore.folders.inboxSent
		},
		{
			name: 'Do',
			attribute: 'to',
			sortable: true,
			width:5,
			filter: {
				type: 'input',
				cellOptions: {
					style: {
						width: '250px'
					}
				}
			},
			visible: folder == MailboxStore.folders.inboxSent
		},
		{
			name: 'Data',
			attribute: 'date',
			sortable: true,
			width:2,
			filter: {
				cellOptions : {
					style: {
						width: '250px'
					}
				},
				items: [
					{
						name: 'date_from',
						type: 'date',
						placeholder: 'od'
					},
					{
						name: 'date_to',
						type: 'date',
						placeholder: 'do'
					}
				]
			},
			value: (data) => {
				return moment(data.date).format('DD-MM-YYYY HH:mm:ss')
			},
		},
		{
			name: 'Rozmair',
			attribute: 'size',
			sortable: false,
			width:5,
			filter: {
				type: 'input',
				cellOptions: {
					style: {
						width: '80px'
					}
				}
			}
		}
	];
}
export let rowAction = {
	settings: {
		width:'120px'
	},
	items: [
		{
			type: 'view_in_new_tab',
			name: 'Otwórz wiadomość w nowej karcie',
			icon: 'external-link',
			link: (item, route) => {
				return '/'+route.params.idMailbox+'/details/'+route.params.folder+'/'+item.uid
			},
			access: true,
			addClass: (rowData) => {
				return 'orange';
			}
		},
		{
			type: 'confirm',
			textConfirm: (rowData) => {
				let { item } = rowData.props

				return 'Czy oznaczyć jako ' + (item.flags['SEEN'] ? 'nieprzeczytana' : 'przeczytana') + ' ?'
			},
			name: (rowData) => {
				let { item } = rowData.props

				return 'Oznacza jako: '+ (item.flags['SEEN'] ? 'nieprzeczytana' : 'przeczytana') +' (obecnie jest '+ (item.flags['SEEN'] ? 'przeczytana' : 'nieprzeczytana') + ')';
			},
			icon: (rowData) => {
				let { item } = rowData.props
				
				if(item.flags['SEEN'])
					return 'eye';
				else{
					return 'low-vision';
				}
					
			},
			value: (componentRow, route) => {
				let { uid, folder } = componentRow.props.item

				MailboxStore.mark('seen', route.params.idMailbox, folder, uid, componentRow.props.changeRow, componentRow.props.item)
			},
			access: true,
			addClass: (rowData) => {
				let { item } = rowData.props
				
				if(item.flags['SEEN'])
					return 'green';
				else
					return 'red';
			}
		}
	]
};
