import React, {Component} from 'react';
import _ from 'lodash';
import {observer} from 'mobx-react';
import { withRouter } from 'react-router';
import { List, Card, Icon  } from 'ui';
import Loading from 'components/Loading';
import {default as Folder } from 'components/Folder';

// Stores
import UserStore from 'stores/User';
import MailboxStore from './stores/model';

// Components
import BaseComponent from 'components/BaseComponent';
import { Input, Menu, Segment, Button } from 'ui';

@observer
class ListFolder extends BaseComponent {

	constructor(props) {
    super(props);
		this.id
    this.breadcrumb = [{
      route: 'user',
      name: 'Użytkownicy'
    }];
  }

	componentWillMount () {
    MailboxStore.loadFolders(this.props.match.params.idMailbox)
  }

	componentWillReceiveProps (nextProps) {
		if(this.props.match.params.idMailbox != nextProps.match.params.idMailbox)
			MailboxStore.loadFolders(nextProps.match.params.idMailbox)
  }

	renderFolders () {
		let mailbox = _.find(UserStore.mailboxes, { id : this.props.match.params.idMailbox })

		return(
			<Card style={{'width' : '100%'}}>
				<Card.Content className="header-mail">
           <Icon name='user' size="large" style={{'float' : 'left'}} />
	         <Card.Header style={{'marginTop' : '0px'}}>
	           { mailbox.username }
	         </Card.Header>
	      	</Card.Content>
			    <Card.Content className="content-folders">
						<Folder data={ MailboxStore.folders.mailboxes } />
					</Card.Content>
			  </Card>
    )
	}

	render() {
		return (
      <div id="list-folders" style={{ 'position' : 'relative'}}>
				{
					!MailboxStore.isLoadingFolders ? this.renderFolders() : <Loading style={{ 'top' : '0px'}} text="Trwa pobieranie folderów" />
				}
			</div>
		)
	}
}

export default withRouter(ListFolder)
