import {observable, action, reaction, toJS} from 'mobx'
import api from 'utils/api'
import _ from 'lodash'
//stores
import Model from '../../../stores/Model'
import NoticeStore from 'components/Notice/store'

export default new class MailboxStore extends Model{
  @observable isLoadingFolders = false
  @observable isLoadingMailboxContent = false
  @observable folders = []
  @observable mail = {}
  @observable isSended = false
  @observable files = []
  @observable filesExists = []
  @observable isUploadingFiles = false

  urlsToCrud = {
    getModel : '',
    create : '',
    update : '',
    delete : ''
  }

  uuid = Math.round(Math.random() * 1000000)

  loadingText = ''

  @action async loadFolders (id) {
    this.isLoadingFolders = true

    try {
      let folders = await api('mailbox/mailboxFolders/id/'+id).get()

      this.folders = folders
      this.isLoadingFolders = false
    } catch (err) {

    }
  }

  @action async getMail(idMailbox, folder, idMail) {
    this.isLoadingMailboxContent = true

    try {
      let mail = await api('mailbox/getMail/idMailbox/'+idMailbox+'/folder/'+folder+'/idMail/'+idMail).get()
      this.mail = mail

      this.isLoadingMailboxContent = false
    } catch (err) {
      this.isLoadingMailboxContent = false
    }
  }

  @action async getCountNewMails()
  {
    try {
      let mailboxes = await api('mailbox/getCountNewMails').get()


      this.isLoadingMailboxContent = false
    } catch (err) {
      this.isLoadingMailboxContent = false
    }    
  }

  @action async sendMail(idMailbox, form) {
    let address = ''
    this.loadingText = 'Trwa wysyłanie'
    this.isLoadingMailboxContent = true

    try {
      if(form.idMail != undefined)
        address = 'mailbox/sendMail/idMailbox/'+idMailbox+'/hashAttachment/'+this.uuid+'/idMail/'+form.idMail+'/folder/'+form.folder
      else
        address = 'mailbox/sendMail/idMailbox/'+idMailbox+'/hashAttachment/'+this.uuid

      let mail = await api(address).post(form.values())
      this.mail = mail

      NoticeStore.push({
        'type' : 'success',
        'text' : 'E-mail został pomyślnie wysłany'
      })

      this.loadingText = ''
      this.isLoadingMailboxContent = false

      this.isSended = true
      this.files.clear()
      this.filesExists.clear()
    } catch (err) {
      this.isLoadingMailboxContent = false
    }
  }

  @action async composeMail (type, idMailbox, folder, idMail) {
    this.isLoadingMailboxContent = true

    return this['compose'+_.startCase(type)](idMailbox, folder, idMail);
  }

  async composeReply (idMailbox, folder, idMail) {
    try {
      let mail = await api('mailbox/compose/idMailbox/'+idMailbox+'/folder/'+folder+'/idMail/'+idMail+'/type/reply').get()
      this.mail = mail

      this.isLoadingMailboxContent = false

      return new Promise((resolve, reject) => {
          resolve(mail);
      });
    } catch (err) {
      this.isLoadingMailboxContent = false
    }
  }

  async composeForward (idMailbox, folder, idMail) {
    try {
      let mail = await api('mailbox/compose/idMailbox/'+idMailbox+'/folder/'+folder+'/idMail/'+idMail+'/type/forward/hash/'+this.uuid).get()
      this.mail = mail
      this.filesExists = mail.files

      this.isLoadingMailboxContent = false

      return new Promise((resolve, reject) => {
          resolve(mail);
      });
    } catch (err) {
      this.isLoadingMailboxContent = false
    }
  }

  @action async mark (type, idMailbox, folder, idMail, reload = false, item) {
    if(reload)
      this.isLoadingMailboxContent = true
    
    item.flags['SEEN'] = !item.flags['SEEN'];

    await this['mark'+_.startCase(type)](idMailbox, folder, idMail, item);
    NoticeStore.push({
      'type' : 'success',
      'text' : 'Wiadomość została pomyślnie oznaczona jako: '+(item.flags['SEEN'] ? 'przeczytana' : 'nieprzeczytana')
    })

    if(reload){
      reload(item, false)
      this.isLoadingMailboxContent = false
    }
      
  }

  async markSeen (idMailbox, folder, idMail, item) {

    try {
      let mail = await api('mailbox/setFlag/flag/seen/val/'+(item.flags['SEEN'] ? 1 : 0)+'/idMailbox/'+idMailbox+'/folder/'+folder+'/idMail/'+idMail).get()
      //this.mail = mail.model

      return new Promise((resolve, reject) => {
          resolve(mail);
      });
    } catch (err) {
      this.isLoadingMailboxContent = false
    }
  }

  async upload () {
   //start upload files
   if(this.files.length > 0)
   {
     this.isUploadingFiles = true;
     let countUploadedFiles = 0;

     this.files.map( (file, index) => {
       file.isUploading = true
       let promise = this.requestUpload(file.objectFile, this.uuid);
       promise.then((response)=>{
         file.isUploading = false
         file.isUploaded = true

         this.filesExists.push(response.file)

         NoticeStore.push({
           'type' : 'success',
           'text' : 'Pliki został wgrany: '+file.objectFile.name
         })

         countUploadedFiles++

         if(countUploadedFiles == this.files.length){
           NoticeStore.push({
             'type' : 'success',
             'text' : 'Wszystkie pliki zostay wgrane'
           })
           this.isUploadingFiles = false

           this.files.clear()
         }
       })
     })
   }
 }

  async requestUpload(file, uuid)
  {
    var data = new FormData();
    data.append('file', file);
    let response = await api('mailbox/uploadAttachment/hash/'+uuid).post(data);

    return new Promise((resolve, reject) => {
        resolve(response);
    });
  }

  getLoadingText () {
    return this.loadingText != '' ? this.loadingText : 'Trwa ładowanie danych'
  }

}
