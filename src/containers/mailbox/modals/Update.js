import React, {Component} from 'react'
import {observer} from 'mobx-react'

// Components
import Modal from 'components/modalcontent';
import { Form, Button } from 'ui';

// Form
import form from '../forms/Update';
import FormContent from '../forms/Content';

//stores
import User from '../stores/model';

import { fields } from '../forms/fields';

export default class Update extends Modal {

	constructor (props) {
		super(props);
		this.initForm = this.initForm.bind(this);
	}

	init() {
		form.modal = this
	}

	initForm (response) {
		form.init(fields('update', response));
	}

	async getData () {
		await User.getModel(this.props.data.product, this.initForm)
	}

	getHeader () {
		return (
			<Modal.Header>Edycja użytkownika: { User.model.display_name }</Modal.Header>
		)
	}

	getContent () {
		return (
	    <Modal.Content>
	      <Modal.Description>
					<Form error={!form.isValid}>
						<FormContent form={form} />
					</Form>
	      </Modal.Description>
	    </Modal.Content>
		)
	}

	getActions () {
		return (
			<Modal.Actions>
				<Button negative icon='remove' labelPosition='right' content='Wyczyść' onClick={form.onClear} />
				<Button positive icon='checkmark' labelPosition='right' content='Zapisz' onClick={form.onSubmit} />
			</Modal.Actions>
		)
	}
}
