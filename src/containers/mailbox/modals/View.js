import React, {Component} from 'react';
import {observer} from 'mobx-react';

// Components
import Modal from 'components/modalcontent';

//stores
import User from '../stores/model';

export default class View extends Modal {

	async getData () {
		await User.getModel(this.props.data.user);
	}

	getHeader () {
		return (
			<Modal.Header>Szczegóły użytkownika: { User.model.display_name }</Modal.Header>
		)
	}

	getContent () {
		return (
	    <Modal.Content>
	      <Modal.Description>
					<div className="wrap-details-item">
            <div className="row">
              <div className="label">Nazwa wyświetlana</div>
              <div className="value">{User.model.display_name}</div>
            </div>
            <div className="row">
              <div className="label">E-mail</div>
              <div className="value">{User.model.email}</div>
            </div>
            <div className="row">
              <div className="label">Role</div>
              <div className="value">{User.model.role}</div>
            </div>
          </div>
	      </Modal.Description>
	    </Modal.Content>
		)
	}

	getActions () {
		return null
	}
}
