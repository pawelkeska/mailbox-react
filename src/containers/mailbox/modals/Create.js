import React, {Component} from 'react';
import {observer} from 'mobx-react';

// Components
import Modal from 'components/modalcontent';
import { Form, Button } from 'ui';
import { translate } from 'components/Language';

// Form
import form from '../forms/Create';
import FormContent from '../forms/Content';

//stores
import User from '../stores/model';

import { fields } from '../forms/fields';

export default class Crate extends Modal {

	constructor (props) {
		super(props);
		this.initForm = this.initForm.bind(this);
	}

	async getData () {
		await User.getOptions(this.initForm)
	}

	initForm (response) {
		form.init(fields('create', response));
	}

	init () {
		form.clear();
		form.modal = this
	}

	getHeader () {
		return (
			<Modal.Header>Nowy użytkownik</Modal.Header>
		)
	}

	getContent () {
		return (
	    <Modal.Content>
	      <Modal.Description>
					<Form error={!form.isValid}>
						<FormContent form={form} />
					</Form>
	      </Modal.Description>
	    </Modal.Content>
		)
	}

	getActions () {
		return (
			<Modal.Actions>
				<Button negative icon='remove' labelPosition='right' content='Wyczyść' onClick={form.onClear} />
				<Button positive icon='checkmark' labelPosition='right' content='Zapisz' onClick={form.onSubmit} />
			</Modal.Actions>
		)
	}
}
