import React, {Component} from 'react';
import {observer} from 'mobx-react';

// Components
import Modal from 'components/modalcontent';
import { Button } from 'ui';

//stores
import User from '../stores/model';

export default class ConfirmRegistration extends Modal {

	async getData () {
		await User.getRegistration(this.props.data.user);
	}

	async _confirmRegistration (create) {
		this.setLoading(true)
    let response = await User.confirmRegistration(this.props.data.user, create)
		this.setLoading(false)
    //ModalStore.close(this.props.uuid)
  }

	getHeader () {
		if(!User.isConfirm && User.registration.type)
			return (
				<Modal.Header>Czy na pdostawie tych danych utworzyć: { User.registration.type }</Modal.Header>
			)
		else
			return (
				<Modal.Header>Potwierdzenie</Modal.Header>
			)
	}

	getContent () {
		if(!User.isConfirm)
			if(User.registration.type)
				return (
			    <Modal.Content>
			      <Modal.Description>
							<div className="wrap-details-item">
								<div className="row">
									<div className="label">Rodzaj współpracy</div>
									<div className="value">{User.registration.type}</div>
								</div>
								<div className="row">
									<div className="label">Nazwa firmy</div>
									<div className="value">{User.registration.name}</div>
								</div>
								<div className="row">
									<div className="label">Miasto</div>
									<div className="value">{User.registration.city}</div>
								</div>
								<div className="row">
									<div className="label">Ulica</div>
									<div className="value">{User.registration.street}</div>
								</div>
								<div className="row">
									<div className="label">Kod pocztowy</div>
									<div className="value">{User.registration.post_code}</div>
								</div>
								<div className="row">
									<div className="label">NIP</div>
									<div className="value">{User.registration.nip}</div>
								</div>
								<div className="row">
									<div className="label">Telefon</div>
									<div className="value">{User.registration.phone}</div>
								</div>
								<div className="row">
									<div className="label">E-mail</div>
									<div className="value">{User.registration.email}</div>
								</div>
								<div className="row">
									<div className="label">Opis</div>
									<div className="value">{User.registration.comments}</div>
								</div>
							</div>
			      </Modal.Description>
			    </Modal.Content>
				)
			else
			return (
				<Modal.Content>
					<Modal.Description>
						Użytkownik nie rejestrował się poprzez formularz, nie ma przypisanych danych dostawcy lub odbiorcy
					</Modal.Description>
				</Modal.Content>
			)
		else
			return (
				<Modal.Content>
					<Modal.Description>
						Użytkownik został już potwierdzony
					</Modal.Description>
				</Modal.Content>
			)
	}

	getActions () {
		if(!User.isConfirm)
			return (
				<Modal.Actions>
					{ User.registration.type ? <Button color="blue" icon='remove' labelPosition='right' content='Dodaj i potwierdź' onClick={() => this._confirmRegistration(true)} /> : null }
					<Button color="green" icon='checkmark' labelPosition='right' content='Potwierdź' onClick={() => this._confirmRegistration(false)} />
				</Modal.Actions>
			)
		else
			return null
	}
}
