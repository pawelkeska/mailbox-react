export const fields = [
  {
    name: 'to',
    label: 'Do',
    placeholder: 'Proszę wpisać adres e-mail',
    rules: 'required',
  },
  {
    name: 'cc',
    label: 'Kopia',
    placeholder: 'Proszę wpisać adres e-mail',
  },
  {
    name: 'bcc',
    label: 'Ukryta kopia',
    placeholder: 'Proszę wpisać adres e-mail',
  },
  {
    name: 'title',
    label: 'Temat',
    placeholder: 'Proszę wpisać tytuł wiadomości',
    rules: 'required',
  },
  {
    name: 'description',
    label: 'Treść',
    placeholder: 'Proszę wpisać treść',
    rules: 'required'
  },
  {
    name: 'withMailFooter',
    value: 0
  },
  {
    name: 'typeMessage',
    value: 'create',
  },
  {
    name: 'template_footer',
    options: [
      {
        isSelected: true,
        value: 1,
        name: 'NCK',
      },
      {
        isSelected: false,
        value: 2,
        name: 'JAMADU',
      }
    ]
  }  
];
