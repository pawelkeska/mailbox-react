import MobxReactForm from 'mobx-react-form';
import validatorjs from 'validatorjs';
//stores
import MailboxStore from '../../stores/model';

import { fields } from './fields';

class Create extends MobxReactForm {

  /*
    Below we are returning a `plugins` object using the `validatorjs` package
    to enable `DVR` functionalities (Declarative Validation Rules).
  */
  plugins() {
    return { dvr: validatorjs };
  }

  /*
    Return the `fields` as a collection into the `setup()` method
    with a `rules` property for the validation.
  */
  setup() {
    return {
      fields: fields
    }
  }

  /*
    Event Hooks
  */
  hooks() {
    return {
      /*
        Success Validation Hook
      */
      onSuccess(form) {
        MailboxStore.sendMail(form.idMailbox, form)
      },
      /*
        Error Validation Hook
      */
      onError(form) {

      }
    };
  }
}
  const form = new Create()
  export default form
