import React, {Component} from 'react';
import {observer} from 'mobx-react';
import { withRouter } from 'react-router'
import {observable} from 'mobx'
import Config from 'config'
import Input from 'components/Form/Input';
import Textarea from 'components/Form/Textarea';
import DropdownSearch from 'components/Form/DropdownSearch'
import { Button, Icon, Segment, Grid, Label, Confirm } from 'ui';
import Loading from 'components/Loading'
import Uploader from 'components/Uploader';
import Files from '../components/File'
import Editor from '../../components/Editor'
import Multiselect from 'components/Form/Select';
//stores
import UserStore from 'stores/User';
import MailboxStore from '../../stores/model';


@observer
class Form extends Component {

  @observable visible = {
    cc: false,
    bcc: false,
  }

	constructor (props) {
		super(props);

    this.state = {
      open : false
    }
    this.showInput = this.showInput.bind(this)
    this.showInput = this.showInput.bind(this)
	}

  showInput (type) {
    this.visible[type] =  !this.visible[type]

    if(!this.visible[type])
      this.props.form.$(type).clear();
  }

  submitForm () {
    let { form } = this.props

    form.validate({ showErrors: true }).then(({ isValid }) => {
      if(isValid)
        if(MailboxStore.files.length > 0)
          this.setState({open:true})
        else
          this.props.form.submit()
    })
  }

  componentWillMount () {
    MailboxStore.isSended = false
  }

  renderUploader () {
    if(MailboxStore.isUploadingFiles)
      return <Loading style={{ 'top' : '0px'}} text="Trwa wgrywanie plików" />
    else
      return(
        <div>
          <Uploader store={MailboxStore} showUploadButton={true} />
          <h4>Wgrane pliki</h4><Files items={MailboxStore.filesExists} fileUrl={Config.filesUrl+'mailbox/'+(MailboxStore.uuid)} />
        </div>
      )
  }

  renderSelectFooter()
  {
    let { form, withMailFooter } = this.props

    console.log('TEMPLATES ')
    console.log(UserStore.templatesFooter);

    if(withMailFooter && UserStore.templatesFooter.length > 1) {
      form.$('template_footer').set('rules', 'required')
      return(
        <div className="field-template-footer">
          <label>Stopka</label>
          <div className="select-tempalate-footer">
            <Multiselect
              field={form.$('template_footer')}
              single={true}
            />
          </div>
          {
            form.$('template_footer').error ? 
            (<div className="ui error message" style={{'width' : '100%', 'display' : 'inline-block', 'clear' : 'both' }}>
              <div className="content">
                <p>Pole jest wymagane.</p>
              </div>
            </div>) : ''
          }
          
        </div>
      )
    }
  }

  renderForm () {
    let { form } = this.props

    return (
      <Grid>
        <Grid.Row style={{ 'paddingBottom' : '0px'}}>
          <Grid.Column width={16}>
            <Segment inverted color="blue" clearing style={{'padding' : '8px 10px 0px 10px'}}>
              <div className="menu-mailbox">
                <div className="option">
                  <div className="option" onClick={() => this.props.goBack()}>
                    <Icon size='large' circular name='close' />
                    <p>Anuluj</p>
                  </div>
                </div>
                <div className="option" onClick={this.submitForm.bind(this)}>
                  <Icon size='large' circular name='send' />
                  <p>Wyślij</p>
                </div>
              </div>
            </Segment>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row style={{ 'paddingTop' : '10px', 'background' : '#fff'}}>
    				<Grid.Column width={11} style={{ 'paddingRight' : '0px'}}>
              <Segment>
                <DropdownSearch field={form.$('to')} />
                <div className="with-icon" style={{'display' : this.visible.cc ? 'block' : 'none'}}>
                  <DropdownSearch field={form.$('cc')} />
                  <div className="wrap-icon-form-mail">
                    <Icon name='remove user' color='red' onClick={ e => this.showInput('cc')} />
                  </div>
                </div>
                <div className="with-icon" style={{'display' : this.visible.bcc ? 'block' : 'none'}}>
                  <DropdownSearch field={form.$('bcc')} />
                  <div className="wrap-icon-form-mail">
                    <Icon name='remove user' color='red' onClick={ e => this.showInput('bcc')} />
                  </div>
                </div>
                <div className='options-to'>
                  <Label 
                    style={{'cursor' : 'pointer', 'padding' : '4px 5px 4px 5px', 'display' : !this.visible.cc ? 'inline-block' : 'none'}} 
                    onClick={ e => this.showInput('cc')} 
                    size="tiny" 
                    color="orange"
                  >
                    Dodaj kopię
                  </Label>
                  <Label 
                    style={{'cursor' : 'pointer', 'padding' : '4px 5px 4px 5px', 'display' : !this.visible.bcc ? 'inline-block' : 'none'}} 
                    onClick={ e => this.showInput('bcc')} 
                    size="tiny" 
                    color="orange"
                  >
                    Dodaj kopię ukrytą
                  </Label>
                </div>
                { this.renderSelectFooter() }
                <Input field={form.$('title')} />
                <Editor field={form.$('description')} />
              </Segment>
            </Grid.Column>
            <Grid.Column width={5}>
              <Segment>
                <Label size="big" style={{'width' : '100%', 'textTransform' : 'uppercase', 'marginBottom' : '10px'}}>
                  Dodawanie załączników
                </Label>
                { this.renderUploader() }
              </Segment>
            </Grid.Column>
          </Grid.Row>
          <Confirm
            open={this.state.open}
            header='Załączniki nie zostały dodane'
            content="Czy napewno wysłać maila ?"
            onCancel={() => this.setState({ open: false })}
            onConfirm={() => this.props.form.submit()}
            cancelButton="Nie"
            confirmButton="Tak"
          />
        </Grid>
    )
  }

	render () {
    return (
      <div>
      {
        MailboxStore.isSended ? (
            this.props.goBack()
          ) : (
            !MailboxStore.isLoadingMailboxContent ? this.renderForm() : <Loading style={{ 'top' : '0px'}} text={MailboxStore.getLoadingText()} />
          )
      }
      </div>
    )
	}
}

export default withRouter(Form);
