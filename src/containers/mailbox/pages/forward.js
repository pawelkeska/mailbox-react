import React, {Component} from 'react';
import {observer} from 'mobx-react';
import { withRouter } from 'react-router'

// Components
import Modal from 'components/modalcontent';
import { Form, Button } from 'ui';

// Form
import FormMobx from './forms/Create';
import FormContent from './forms/Content';

//stores
import MailboxStore from '../stores/model';

import { fields } from './forms/fields';

@observer
class Forward extends Component {

	constructor (props) {
		super(props);
	}

	async componentWillMount () {
	FormMobx.idMailbox = this.props.match.params.idMailbox;
	FormMobx.idMail = this.props.match.params.idMail;
	FormMobx.folder = this.props.match.params.folder;
	FormMobx.clear();

	let upPromise = MailboxStore.composeMail('forward', this.props.match.params.idMailbox, this.props.match.params.folder, this.props.match.params.idMail)

    upPromise.then((mail) => {
      console.log(mail)
			let fromAdresses = []

			if(mail.from.length > 0)
				mail.from.map((from) => {
					fromAdresses.push(from.mailto)
				})

			mail.to = fromAdresses.join('; ')
			FormMobx.set({typeMessage : 'forward'})
			FormMobx.update(mail)
		})
	}

	render () {
		return (
			<Form error={!FormMobx.isValid} id="form-new-mail">
				<FormContent form={FormMobx} />
			</Form>
		)
	}
}

export default withRouter(Forward);
