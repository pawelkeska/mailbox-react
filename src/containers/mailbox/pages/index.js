import React, {Component} from 'react'
import {observer} from 'mobx-react'
import { withRouter } from 'react-router'
import {NavLink} from 'react-router-dom'

// Stores
import ModalStore from '../../../stores/Modal'
import ProductStore from '../stores/model'

// Components
import BaseComponent from 'components/BaseComponent'
import { Icon, Image, Reveal, Button } from 'ui'
import Grid from '../../../components/Grid/Grid'

//Modals
import ViewModal from '../modals/View'

//settings to grid
import { rowAction, columns } from '../grid';

@observer
class GridMails extends BaseComponent {

  constructor(props) {
    super(props)

    this.state = {
      countSelectedMails : 0
    }

    this.afterSelectRowInGrid = this.afterSelectRowInGrid.bind(this)
  }

  componentDidUpdate(prevProps, prevState){
		if(this.props.match.params.idMailbox != prevProps.match.params.idMailbox|| this.props.match.params.folder != prevProps.match.params.folder){
      this.grid.reload()
    }
  }

  showDetailsMailModal(data) {
    ModalStore.push(ViewModal, {
      data
    });
  }

  afterSelectRowInGrid(count) {
    this.setState({
      countSelectedMails: count
    })
  }

  renderRestOptions () {
    if(this.state.countSelectedMails > 0)
      return (
        <div style={{'display' : 'inline-block'}}>
          <div className="option">
            <NavLink to={'/'+this.props.match.params.idMailbox+'/new'}>
              <Icon size='large' circular name='reply' />
              <p>Odpowiedz</p>
            </NavLink>
          </div>
          <div className="option">
            <NavLink to={'/'+this.props.match.params.idMailbox+'/new'}>
              <Icon size='large' circular name='mail forward' />
              <p>Przekaż</p>
            </NavLink>
          </div>
        </div>
      )
    else
      return (
        <div className="no-selected">
          <Button size="tiny" color="green">Zaznacz e-mail, aby wyświetlić więcej opcji</Button>
        </div>
      )
  }

	render() {

		return (
      <div>
        <div className="menu-mailbox">
          <div className="option" onClick={() => this.grid.reload()}>
            <Icon size='large' circular name='refresh' />
            <p>Odśwież</p>
          </div>
          <div className="option">
            <NavLink to={'/'+this.props.match.params.idMailbox+'/new'}>
              <Icon size='large' circular name='mail outline' />
              <p>Utwórz</p>
            </NavLink>
          </div>
          { this.renderRestOptions() }
        </div>
				<Grid
					onRef={instance => { this.grid = instance; }}
					url={'mailbox/getMails/id/'+this.props.match.params.idMailbox+'/folder/'+this.props.match.params.folder}
					class={{'mailbox-grid' : true}}
					columns={columns}
          handleDoubleClickRow={this.showDetailsMailModal}
          afterSelect={this.afterSelectRowInGrid}
				/>
			</div>
		)
	}
}

export default withRouter(GridMails)
