import React, {Component} from 'react'
import {observable} from 'mobx';
import {observer} from 'mobx-react'
import { withRouter } from 'react-router'
import moment from 'moment'
import {NavLink} from 'react-router-dom'
import Config from 'config'
// Stores
import MailboxStore from '../stores/model'
// Components
import BaseComponent from 'components/BaseComponent'
import Loading from 'components/Loading'
import 'images/blocked.gif'

import { Header, Segment, Icon, Label, Grid, Card } from 'ui'

@observer
class DetailsMessage extends BaseComponent {
  @observable isLoadingBar = false

  constructor (props) {
    super(props)

    this.clickMark = this.clickMark.bind(this)
  }

  async componentWillMount () {
    await MailboxStore.getMail(this.props.match.params.idMailbox, this.props.match.params.folder, this.props.match.params.idMail)
  }

  renderDetails () {
    return (
      <div>
        { this.renderActionBar() }
      <div id="mailbox-details">
          <Label 
            color={MailboxStore.mail.flags['SEEN'] != undefined && MailboxStore.mail.flags['SEEN'] ? 'green' : 'red'} 
            className="subject-mail" 
            size="big">
              { MailboxStore.mail.subject ? MailboxStore.mail.subject.value : 'Brak tematu' }
          </Label>
          <Header className="header-details" as='div' attached='top'>
            <Icon name='user circle outline' />
            <Header.Content style={{'width' : '100%'}}>
             <Header.Subheader>
                <div className="wrap-item">
                  <label className="name-label">Od:</label> <div className="value" dangerouslySetInnerHTML={{__html: MailboxStore.mail.from.value }}></div>
                </div>
                <div className="wrap-item">
                  <label className="name-label">Do:</label>
                  <div className="value" dangerouslySetInnerHTML={{__html: MailboxStore.mail.to.value}}></div>
                </div>
                <div className="wrap-item">
                  <label className="name-label">Data:</label>
                  <div className="value">
                  { moment(MailboxStore.mail.date.raw).isSame(moment(), 'day')
                   ? 'Dzisiaj '+moment(MailboxStore.mail.date.raw).format('HH:mm')
                   :
                   moment(MailboxStore.mail.date.raw).format('DD-MM-YYYY HH:mm:ss')
                  }
                  </div>
                </div>
                {
                 MailboxStore.mail.priority ? (
                   <div className="wrap-item">
                     <label className="name-label">Priorytet:</label>
                      <div className="value" dangerouslySetInnerHTML={{__html: MailboxStore.mail.priority.value}}>

                      </div><br />
                    </div>
                 ) : ''
                }
             </Header.Subheader>
            </Header.Content>
          </Header>
        <Segment attached>
          <Grid>
            <Grid.Column width={ MailboxStore.mail.attachments ? 10 : 16 }>
              <p dangerouslySetInnerHTML={{__html:  MailboxStore.mail.textHtml}} />
            </Grid.Column>
            {
              MailboxStore.mail.attachments ?
              <Grid.Column width={6} className="list-files">
                <Card style={{'width' : '100%'}}>
                  <Card.Content header="Załączniki" />
                  <Card.Content>
                    <p dangerouslySetInnerHTML={{__html:  MailboxStore.mail.attachments}} />
                  </Card.Content>
                </Card>
              </Grid.Column>
               : null
            }
          </Grid>
        </Segment>
      </div>
      </div>
    )
  }

  async clickMark () {
    let { idMail, idMailbox, folder } = this.props.match.params
    this.isLoadingBar = true

    await MailboxStore.mark(
      'seen', 
      idMailbox, 
      folder, 
      idMail, 
      false, 
      MailboxStore.mail)

    this.isLoadingBar = false
  }

  renderActionBar () {
    if(this.isLoadingBar)
      return (
        <div id="list-folders" style={{ 'position' : 'relative'}}>
          <Loading style={{ 'top' : '0px'}} text="Trwa zapisywanie" />
        </div>
      )
    else
      return (
      <Segment inverted color="blue" clearing style={{'padding' : '8px 10px 0px 10px', 'marginBottom' : '0px'}}>
        <div className="menu-mailbox">
          <div className="option">
            <div onClick={() => this.props.goBack()}>
              <Icon size='large' circular name='close' />
              <p>Wstecz</p>
            </div>
          </div>
          <div className="option">
            <div onClick={this.clickMark}>
              <Icon size='large' circular name={MailboxStore.mail.flags['SEEN'] ? 'low vision' : 'eye'} />
              <p>Oznacz jako: <br /> { MailboxStore.mail.flags['SEEN'] ? 'nieprzeczytaną' : 'przeczytaną' }</p>
            </div>
          </div>
          <div className="option">
            <NavLink to={'/'+this.props.match.params.idMailbox+'/reply/'+this.props.match.params.folder+'/'+this.props.match.params.idMail}>
              <Icon size='large' circular name='reply' />
              <p>Odpowiedz</p>
            </NavLink>
          </div>
          <div className="option">
            <NavLink to={'/'+this.props.match.params.idMailbox+'/forward/'+this.props.match.params.folder+'/'+this.props.match.params.idMail}>
              <Icon size='large' circular name='mail forward' />
              <p>Przekaż</p>
            </NavLink>
          </div>
            <div className="option">
              <a href={Config.urlApi + 'mailbox/printMail/idMailbox/'+this.props.match.params.idMailbox+'/folder/'+this.props.match.params.folder+'/idMail/'+this.props.match.params.idMail} target="_blank">
                <Icon size='large' circular name='print' />
                <p>Drukuj</p>
              </a>
            </div>
        </div>
      </Segment>)
  }

	render() {
		return (
      <div>
      {
        !MailboxStore.isLoadingMailboxContent ? this.renderDetails() : <Loading style={{ 'top' : '0px'}} text="Trwa pobieranie email" />
      }
      </div>
		)
	}
}

export default withRouter(DetailsMessage)
