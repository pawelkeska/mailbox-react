import React, {Component} from 'react';
import {observer} from 'mobx-react';
import { withRouter } from 'react-router'

// Components
import Modal from 'components/modalcontent';
import { Form, Button } from 'ui';

// Form
import FormMobx from './forms/Create';
import FormContent from './forms/Content';

//stores
import User from '../stores/model';

import { fields } from './forms/fields';

@observer
class Create extends Component {

	constructor (props) {
		super(props);
	}

	componentWillMount () {
		FormMobx.idMailbox = this.props.match.params.idMailbox;
		FormMobx.clear();
		FormMobx.set({withMailFooter : 1})
	}

	render () {
		return (
			<Form onSubmit={FormMobx.onSubmit} error={!FormMobx.isValid} id="form-new-mail">
				<FormContent form={FormMobx} withMailFooter={true} />
			</Form>
		)
	}
}

export default withRouter(Create);
