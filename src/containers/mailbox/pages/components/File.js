import React, {Component} from 'react';
import {observer} from 'mobx-react';
import { Label } from 'ui'
import FontAwesome from 'react-fontawesome';

const Files = props => {
    let { items } = props;

    if(items.length > 0)
  		return (
        <div className="wrap-files">
        {
          items.map((file, index) => {
            return <File url={props.fileUrl+'/'+file.file} key={index} item={file} />
          })
        }
        </div>
      )
    else
      return (<Label color="blue">Brak plików</Label>)
};

const File = props => {
  return (
    <Label color="green">
      <FontAwesome name="file-o" /> <a href={props.url} target="_blank" className="file">{props.item.name}</a>
    </Label>
  )
}

export default observer(Files);
