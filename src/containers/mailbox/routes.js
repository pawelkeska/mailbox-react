import React, {Component} from 'react';
import {Route} from '../../utils/react-router-dom-async';
import {observer} from 'mobx-react';

@observer
export default class Routes extends Component {
	render() {
		return (
			<div>
				<Route path="/:idMailbox/list/:folder" fetch={System.import('containers/mailbox/ListMails')} />
				<Route path="/:idMailbox/new" fetch={System.import('containers/mailbox/pages/new')} />
				<Route path="/:idMailbox/details/:folder/:idMail" fetch={System.import('containers/mailbox/pages/details')} />
				<Route path="/:idMailbox/reply/:folder/:idMail" fetch={System.import('containers/mailbox/pages/reply')} />
				<Route path="/:idMailbox/forward/:folder/:idMail" fetch={System.import('containers/mailbox/pages/forward')} />
			</div>
		)
	}
}
