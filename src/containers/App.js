import React, {Component} from 'react';
import {BrowserRouter, Switch, Link} from 'react-router-dom';
import {Route} from '../utils/react-router-dom-async';
import {observer} from 'mobx-react';
import Notice from 'components/Notice';
import Loading from 'components/Loading';
// languages
import 'utils/moment-locale';
// Add-ons
import LaserBeam from 'react-laser-beam';
// Layouts
import Layout from 'layouts/mailbox/Layout';
// Containers
import ModalContainer from './Modals';
//import TopNav from './TopNav';
// Stores
import AppStore from '../stores/App';
import User from 'stores/User';


@observer
export default class App extends Component {

	componentWillMount () {
		User.getMailboxes();
	}

	render() {
		if(User.isLoading)
			return <Loading text="Trwa ładowanie danych" />
		else
			return (
				//<BrowserRouter>
				<BrowserRouter basename="/backend/mailbox/admin">
					<div>
						<LaserBeam
							zIndex="15000"
							width="5px"
							addon="#d1e3ff"
							background="#a8caff"
							show={AppStore.isLoading} />
							{
								<Layout />
							}
						<ModalContainer />
						<Notice />
					</div>
				</BrowserRouter>
			)
	}
};
