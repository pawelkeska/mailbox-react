import React, {Component} from 'react';
import {observer} from 'mobx-react';

// Stores
import ModalStore from '../stores/Modal';

@observer
class ModalsContainer extends Component {
  render() {
    return (
      <div>
      {
        ModalStore.items.map((Modal, index) => (
          <div key={index}>
            {
                <Modal.component  data={Modal.props} removeFromStore={ () => (ModalStore.close(Modal.uuid)) } />
            }
          </div>
        ))
      }
      </div>
    )
  }
};

export default ModalsContainer;
