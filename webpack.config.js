var path = require("path");
var webpack = require("webpack");
var precss = require("precss");
var autoprefixer = require("autoprefixer");
var HtmlWebpackPlugin = require("html-webpack-plugin");
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: [
        "react-hot-loader/patch",
        "babel-polyfill",
        "whatwg-fetch",
        "webpack-dev-server/client?http://localhost:3000",
        "webpack/hot/only-dev-server",
        "./src/index"
    ],
    output: {
        path: path.join(__dirname, "dist"),
        publicPath: "/",
        filename: "app.[hash].js"
    },
    devtool: "eval",
    resolve: {
      modules: [
        path.resolve('./src'),
        'node_modules'
      ]
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                include: path.join(__dirname, "src"),
                loader: "babel-loader",
                query: {
                    presets: [
                        [ "es2015", { modules: false } ],
                        "stage-0",
                        "react"
                    ],
                    plugins: [
                        "react-hot-loader/babel",
                        "transform-async-to-generator",
                        "transform-decorators-legacy",
                        "lodash",
                        "transform-react-handled-props",
                        ["transform-react-remove-prop-types", {
                          "mode": "wrap"
                        }],
                        ["transform-runtime", {
                          "polyfill": false,
                          "regenerator": false
                        }]
                    ]
                }
            },
            {
                test: /\.scss|css$/i,
                use: ExtractTextPlugin.extract({
                  fallback: 'style-loader',
                  use: [
                    {
                      loader: 'css-loader',
                      options: {
                        sourceMap: true
                      }
                    },
                    {
                      loader: 'resolve-url-loader',
                      options: {
                        keepQuery: true
                      }
                    },
                    {
                      loader: 'postcss-loader',
                      options: {
                        sourceMap: true,
                        plugins: () => [precss, autoprefixer]
                      }
                    },
                    {
                      loader: 'sass-loader',
                      options: {
                        sourceMap: true
                      }
                    }
                  ]
                })
            },
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                  fallback: 'style-loader',
                  use: [
                    {
                      loader: 'css-loader',
                      options: {
                        sourceMap: true
                      }
                    },
                    {
                      loader: 'resolve-url-loader',
                      options: {
                        keepQuery: true
                      }
                    },
                    {
                      loader: 'postcss-loader',
                      options: {
                        sourceMap: true,
                        plugins: () => [precss, autoprefixer]
                      }
                    },
                    {
                      loader: 'less-loader',
                      options: {
                        sourceMap: true
                      }
                    }
                  ]
                })
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: ['file-loader?context=src/images&name=images/[path][name].[ext]', {
                  loader: 'image-webpack-loader',
                  query: {
                    mozjpeg: {
                      progressive: true,
                    },
                    gifsicle: {
                      interlaced: false,
                    },
                    optipng: {
                      optimizationLevel: 4,
                    },
                    pngquant: {
                      quality: '75-90',
                      speed: 3,
                    },
                  },
                }],
                exclude: /node_modules/,
                include: __dirname,
            },
            { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&mimetype=application/font-woff" },
            { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },
        ]
    },
    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({ hash: false, template: "./index.hbs" }),
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /nb/),
        new ExtractTextPlugin({
          filename: '[name].css',
          allChunks: true
        })
    ]
};
