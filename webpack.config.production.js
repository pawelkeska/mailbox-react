var path = require('path');
var webpack = require('webpack');
var autoprefixer = require('autoprefixer');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
		entry: {
			context: path.resolve(__dirname, "src")
		},
    output: {
        path: path.join(__dirname, 'dist'),
        publicPath: '/',
        filename: '[name].js'
    },
    resolve: {
      modules: [
        path.resolve('./src'),
        'node_modules'
      ]
    },
    module: {
        loaders: [{
            test: /\.js$/,
            include: path.join(__dirname, 'src'),
            loader: 'babel-loader',
            query: {
                "presets": [[ "es2015", { modules: false } ], "stage-0", "react"],
                "plugins": [
                  'transform-async-to-generator',
                  'transform-decorators-legacy',
                  "lodash",
                  "transform-react-handled-props",
                  ["transform-react-remove-prop-types", {
                    "mode": "wrap"
                  }],
                  ["transform-runtime", {
                    "polyfill": true,
                    "regenerator": true
                  }]
                ]
            }
        }, {
            test: /\.scss$/i,
            use: ExtractTextPlugin.extract({
              fallback: 'style-loader',
              use: [
                {
                  loader: 'css-loader',
                  options: {
                    sourceMap: false,
                    minimize: true,
                    discardComments: {
                      removeAll: true
                    }
                  }
                },
                {
                  loader: 'resolve-url-loader'
                },
                {
                  loader: 'postcss-loader',
                  options: {
                    plugins: () => [autoprefixer]
                  }
                },
                {
                  loader: 'sass-loader',
                  options: {
                    sourceMap: false
                  }
                }
              ]
            })
        }, {
            test: /\.less$/,
            use: ExtractTextPlugin.extract({
              fallback: 'style-loader',
              use: [
                {
                  loader: 'css-loader',
                  options: {
                    sourceMap: false,
                    minimize: true,
                    discardComments: {
                      removeAll: true
                    }
                  }
                },
                {
                  loader: 'resolve-url-loader'
                },
                {
                  loader: 'postcss-loader',
                  options: {
                    plugins: () => [autoprefixer]
                  }
                },
                {
                  loader: 'less-loader',
                  options: {
                    sourceMap: false
                  }
                }
              ]
            })
        }, {
            test: /\.(jpe?g|png|gif|svg)$/i,
            loaders: ['file-loader?context=src/images&name=images/[path][name].[ext]', {
              loader: 'image-webpack-loader',
              query: {
                mozjpeg: {
                  progressive: true,
                },
                gifsicle: {
                  interlaced: false,
                },
                optipng: {
                  optimizationLevel: 4,
                },
                pngquant: {
                  quality: '75-90',
                  speed: 3,
                },
              },
            }],
            exclude: /node_modules/,
            include: __dirname,
        },  {
          test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: "url-loader?limit=10000&mimetype=application/font-woff"
        },
        {
          test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: "file-loader"
        }
      ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.optimize.OccurrenceOrderPlugin(true),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        }),
        new ExtractTextPlugin("styles.css"),
        new HtmlWebpackPlugin({
            hash: false,
            template: './index.hbs'
				}),
				new webpack.optimize.LimitChunkCountPlugin({
					maxChunks: 1,
				}),
    ],
};
